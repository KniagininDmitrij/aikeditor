def prov_zap2db(connection, batch_number, plate_number, test_id, last_pred_id, flag_stop):

    test_id_new = []
    stat_zapdon = 0

    with connection.cursor() as cur:
        cur.execute("SELECT id FROM atedb.testplan_results WHERE batch_number = '" + str(batch_number) + "' AND plate_number = '" + str(plate_number) + "'")
        testplan_results = cur.fetchall()[0]

    if testplan_results.get('id') == last_pred_id + 1 and flag_stop == 0:
        with connection.cursor() as cur:
            cur.execute("SELECT test_id FROM atedb.measurements WHERE testplan_results = " + str(testplan_results.get('id')))
            bufer = cur.fetchall()

        for i in range(0, len(bufer)):
            test_id_new.append(bufer[i].get('test_id'))

        if len(test_id) == len(test_id_new):
            for h in range(0, len(test_id)):
                if test_id[h] != test_id_new[h]:
                    stat_zapdon = 1
        else:
            stat_zapdon = 1
    elif testplan_results.get('id') == last_pred_id + 1 and flag_stop == 1:
        stat_zapdon = 0
    else:
        stat_zapdon = 1

    return stat_zapdon
