from sys import argv
from funcs import runtest
from funcs import printVACs
import time

# входные параметры при запуске скрипта
testStructure = [["C:\\TestInstruments\\AIK\\XML\\npn.xml", 5],
                 # ["C:\\TestInstruments\\AIK\\XML\\BigDiode.xml", 1],
                 # ["C:\\TestInstruments\\AIK\\XML\\npn.xml", 50],
                 ["C:\\TestInstruments\\AIK\\XML\\BigDiode.xml", 15]]

DLLpath = "C:\\TestInstruments\\AIK\\DLL\\SharedLib.dll"
Debug = int(argv[2])
others = None
try:
    xmlPath = argv[1]
    count = float(argv[3])
    if len(argv) > 4:
        others = [float(i) for i in argv[4:]]
except IndexError:
    Debug = 1


def test():
    for inParams in testStructure:
        DLLpath = "D:\\Documents\\универсальное ПО\\bitbucketUW\\TestInstruments\\AIK\\DLL\\SharedLib.dll"
        xmlPath = inParams[0]
        count = inParams[1]
        if others is None:
            args = [count]
        else:
            args = [count] + others
        error, deskerror = runtest(DLLpath, xmlPath, args, count * 1000)
        if deskerror:
            pass


if Debug:
    test()
else:
    if others is None:
        args = [count]
    else:
        args = [count] + others

    
    error, deskerror = runtest(DLLpath, xmlPath, args, int(count) * 250)
    printVACs()
    # вывод информации об ошибках:
    if int(error) != 0:
        print("Error: {0} - {1}".format(error, deskerror))
