package sample;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystemException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class ResultParser {
    private final static String PATH_OF_RESULT_FILE = "C:\\TestInstruments\\AIK\\Scripts\\results.txt";
    private static final String SPLITTER = ",";
    private static final double DEFAULT_RESULT_VALUE = 0.0;
    private static String INTERNAL_DEFAULT_ENCODING = "windows-1251";
    private static String CONFIG_FILE_NAME = "app.config";
    private static String RESULT_FILE_ENCODING_PROPERTY_NAME = "result.encode";
    private static String RESULT_STRING_ENCODING = getResultStringEncodeFromConfigIfExist();
    private static final int VALUE_INDEX = 0;
    private static final int UNIT_INDEX = 1;
    private static final int MIN_NORM_INDEX = 2;
    private static final int MAX_NORM_INDEX = 3;
    private static final int DIGIT_COUNT_INDEX = 4;
    private static final int BEFORE_POINT_DIGIT_MAX_COUNT = 4;
    private static final int AFTER_POINT_DIGIT_MAX_COUNT = 3;
    private static final Map<String, String> replaceMap = new HashMap(){
        {
            put("--", "+");
        }
    };
    private static ScriptEngineManager mgr = new ScriptEngineManager();
    private static ScriptEngine engine = mgr.getEngineByName("JavaScript");
    private static int currentReturnedCount = 0;

    public static List<Result> getNewResultIfExist(){
        List<Result> resultsList = new ArrayList<>();
        if (!Files.exists(Paths.get(PATH_OF_RESULT_FILE)))
            return resultsList;
        try{
            List<String> resultLines = Files.readAllLines(Paths.get(PATH_OF_RESULT_FILE), Charset.forName(RESULT_STRING_ENCODING)).stream().filter(line -> !line.isEmpty()).collect(Collectors.toList());
            int needParse = resultLines.size() - currentReturnedCount;
            for (int index = 0; index < needParse; index++){
                try {
                    resultsList.add(parseResultObjectFromString(resultLines.get(currentReturnedCount++), "pass"));
                }catch (IndexOutOfBoundsException indexOutOfBoundsException){
                    System.out.printf("Size - %d, index - %d, needParse - %d, currentReturnedCount - %d%n", resultLines.size(), index, needParse, currentReturnedCount);
                }
            }
        } catch (IOException exception) {
            throw new IllegalArgumentException("Incorrect result encoding setting while parsing results.txt");
        }

        return resultsList;
    }

    public static int getCurrentReturnedCount() {
        return currentReturnedCount;
    }

    public static void init() throws IOException, InterruptedException {
        try {
            currentReturnedCount = 0;
            if (Files.exists(Paths.get(PATH_OF_RESULT_FILE)))
                Files.delete(Paths.get(PATH_OF_RESULT_FILE));
        }catch (FileSystemException fileSystemException){
            Thread.sleep(100);
            init();
        }
    }
    private static Result parseResultObjectFromString(String resultString, String headerTextAndParamName){
        try {
            if (!RESULT_STRING_ENCODING.equals(INTERNAL_DEFAULT_ENCODING)){
                resultString = new String(resultString.getBytes(RESULT_STRING_ENCODING), INTERNAL_DEFAULT_ENCODING);
            }
        }catch (Exception encodeException){
            System.out.println(resultString);
        }
        String[] resultStringItems = resultString.split(SPLITTER);
        int afterPointDigitCount = AFTER_POINT_DIGIT_MAX_COUNT;
        if (resultStringItems.length == 5){
            try {
                afterPointDigitCount = Integer.parseInt(resultStringItems[DIGIT_COUNT_INDEX]);
            }catch (Exception e){}
        }
        String value = String.format("%"+ BEFORE_POINT_DIGIT_MAX_COUNT +"." + afterPointDigitCount + "f",
                getValueFromStringExpression(resultStringItems[VALUE_INDEX]));
        String minNorm = resultStringItems[MIN_NORM_INDEX];
        String maxNorm = resultStringItems[MAX_NORM_INDEX];
        String unit = resultStringItems[UNIT_INDEX];
        String paramName = "pass";
        String headerText = "pass";
        return new Result(paramName, headerText, minNorm, maxNorm, unit, value, Methods.getDateTimeString());
    }

    private static double getValueFromStringExpression(String expression){
        String checkedExpression = checkAndReplaceIfNeededByMap(expression, replaceMap);
        try{
            return (double) engine.eval(checkedExpression);
        } catch (ScriptException e) {
            e.printStackTrace();
            return DEFAULT_RESULT_VALUE;
        }
    }

    private static String checkAndReplaceIfNeededByMap(String expression, Map<String, String> replaceMap) {
        String result = expression;
        for (String key : replaceMap.keySet()){
            result = result.replaceAll(key, replaceMap.get(key));
        }
        return result;
    }

    private static String getResultStringEncodeFromConfigIfExist(){
        Properties prop = new Properties();
        String fileName = CONFIG_FILE_NAME;
        try (FileInputStream fis = new FileInputStream(fileName)){
            prop.load(fis);
            return prop.getProperty(RESULT_FILE_ENCODING_PROPERTY_NAME);
        }catch (Exception configReadingException){
            System.out.println("Config file is not correct or not find! - " + configReadingException.getMessage());
            return INTERNAL_DEFAULT_ENCODING;
        }
    }

}
