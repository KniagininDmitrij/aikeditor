package sample;


import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sample.controllers.*;
import sample.enums.DialogEndStatus;
import sample.simple.PathHolder;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.logging.Logger;

public class MainProgramEditWindowController {
    private Map<String, String> currentDataIn = new HashMap<String, String>()
    {
        {
            put("productName", "");
            put("developerName", "");
            //put("adapterCode", "0");
            put("chipSizeX", "1");
            put("chipSizeY", "1");
        };
    };

    //main window

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private FlowPane treeFlowPane;

    @FXML
    private MenuBar menu_buttons_bar;

    @FXML
    private Pane workPane;
    @FXML
    private Label indicateNameLabel;
    @FXML
    private Label indicateStatusLabel;
    @FXML
    private Button movingButt;

    @FXML
    private Button blockButt;

    @FXML
    private Button doubleBlockButt;

    @FXML
    private Button headerButt;

    @FXML
    private Button paramButt;

    @FXML
    private Button allResetButt;

    @FXML
    private Button pauseButt;

    @FXML
    private Button clearAllButt;

    @FXML
    private Button deleteItemButt;

    @FXML
    private Button createVACButt;
    @FXML
    private Button resultButt;
    @FXML
    private Button dataInputButt;

    @FXML
    private Button VSB_I1;
    @FXML
    private Button VMB_I1;
    @FXML
    private Button ISB_I1;
    @FXML
    private Button IMB_I1;
    @FXML
    private Button VSB_I2;
    @FXML
    private Button VMB_I2;
    @FXML
    private Button ISB_I2;
    @FXML
    private Button IMB_I2;
    @FXML
    private Button VSB_I3;
    @FXML
    private Button VMB_I3;
    @FXML
    private Button ISB_I3;
    @FXML
    private Button IMB_I3;
    @FXML
    private Button VSB_I4;
    @FXML
    private Button VMB_I4;
    @FXML
    private Button ISB_I4;
    @FXML
    private Button IMB_I4;
    @FXML
    private ImageView DAQ_img;
    @FXML
    public Circle node_r0c0_1;
    @FXML
    public Circle node_r0c0_2;
    @FXML
    public Circle node_r0c1_1;
    @FXML
    public Circle node_r0c1_2;
    @FXML
    public Circle node_r0c2_1;
    @FXML
    public Circle node_r0c2_2;
    @FXML
    public Circle node_r0c3_1;
    @FXML
    public Circle node_r0c3_2;
    @FXML
    public Circle node_r0c4_1;
    @FXML
    public Circle node_r0c4_2;
    @FXML
    public Circle node_r0c5_1;
    @FXML
    public Circle node_r0c5_2;
    @FXML
    public Circle node_r0c6_1;
    @FXML
    public Circle node_r0c6_2;
    @FXML
    public Circle node_r0c7_1;
    @FXML
    public Circle node_r0c7_2;
    @FXML
    public Circle node_r0c8_1;
    @FXML
    public Circle node_r0c8_2;
    @FXML
    public Circle node_r0c9_1;
    @FXML
    public Circle node_r0c9_2;
    @FXML
    public Circle node_r0c10_1;
    @FXML
    public Circle node_r0c10_2;
    @FXML
    public Circle node_r0c11_1;
    @FXML
    public Circle node_r0c11_2;
    @FXML
    public Circle node_r0c12_1;
    @FXML
    public Circle node_r0c12_2;
    @FXML
    public Circle node_r0c13_1;
    @FXML
    public Circle node_r0c13_2;
    @FXML
    public Circle node_r0c14_1;
    @FXML
    public Circle node_r0c14_2;
    @FXML
    public Circle node_r0c15_1;
    @FXML
    public Circle node_r0c15_2;
    @FXML
    public Circle node_r1c0_1;
    @FXML
    public Circle node_r1c0_2;
    @FXML
    public Circle node_r1c1_1;
    @FXML
    public Circle node_r1c1_2;
    @FXML
    public Circle node_r1c2_1;
    @FXML
    public Circle node_r1c2_2;
    @FXML
    public Circle node_r1c3_1;
    @FXML
    public Circle node_r1c3_2;
    @FXML
    public Circle node_r1c4_1;
    @FXML
    public Circle node_r1c4_2;
    @FXML
    public Circle node_r1c5_1;
    @FXML
    public Circle node_r1c5_2;
    @FXML
    public Circle node_r1c6_1;
    @FXML
    public Circle node_r1c6_2;
    @FXML
    public Circle node_r1c7_1;
    @FXML
    public Circle node_r1c7_2;
    @FXML
    public Circle node_r1c8_1;
    @FXML
    public Circle node_r1c8_2;
    @FXML
    public Circle node_r1c9_1;
    @FXML
    public Circle node_r1c9_2;
    @FXML
    public Circle node_r1c10_1;
    @FXML
    public Circle node_r1c10_2;
    @FXML
    public Circle node_r1c11_1;
    @FXML
    public Circle node_r1c11_2;
    @FXML
    public Circle node_r1c12_1;
    @FXML
    public Circle node_r1c12_2;
    @FXML
    public Circle node_r1c13_1;
    @FXML
    public Circle node_r1c13_2;
    @FXML
    public Circle node_r1c14_1;
    @FXML
    public Circle node_r1c14_2;
    @FXML
    public Circle node_r1c15_1;
    @FXML
    public Circle node_r1c15_2;
    @FXML
    public Circle node_r2c0_1;
    @FXML
    public Circle node_r2c0_2;
    @FXML
    public Circle node_r2c1_1;
    @FXML
    public Circle node_r2c1_2;
    @FXML
    public Circle node_r2c2_1;
    @FXML
    public Circle node_r2c2_2;
    @FXML
    public Circle node_r2c3_1;
    @FXML
    public Circle node_r2c3_2;
    @FXML
    public Circle node_r2c4_1;
    @FXML
    public Circle node_r2c4_2;
    @FXML
    public Circle node_r2c5_1;
    @FXML
    public Circle node_r2c5_2;
    @FXML
    public Circle node_r2c6_1;
    @FXML
    public Circle node_r2c6_2;
    @FXML
    public Circle node_r2c7_1;
    @FXML
    public Circle node_r2c7_2;
    @FXML
    public Circle node_r2c8_1;
    @FXML
    public Circle node_r2c8_2;
    @FXML
    public Circle node_r2c9_1;
    @FXML
    public Circle node_r2c9_2;
    @FXML
    public Circle node_r2c10_1;
    @FXML
    public Circle node_r2c10_2;
    @FXML
    public Circle node_r2c11_1;
    @FXML
    public Circle node_r2c11_2;
    @FXML
    public Circle node_r2c12_1;
    @FXML
    public Circle node_r2c12_2;
    @FXML
    public Circle node_r2c13_1;
    @FXML
    public Circle node_r2c13_2;
    @FXML
    public Circle node_r2c14_1;
    @FXML
    public Circle node_r2c14_2;
    @FXML
    public Circle node_r2c15_1;
    @FXML
    public Circle node_r2c15_2;
    @FXML
    public Circle node_r3c0_1;
    @FXML
    public Circle node_r3c0_2;
    @FXML
    public Circle node_r3c1_1;
    @FXML
    public Circle node_r3c1_2;
    @FXML
    public Circle node_r3c2_1;
    @FXML
    public Circle node_r3c2_2;
    @FXML
    public Circle node_r3c3_1;
    @FXML
    public Circle node_r3c3_2;
    @FXML
    public Circle node_r3c4_1;
    @FXML
    public Circle node_r3c4_2;
    @FXML
    public Circle node_r3c5_1;
    @FXML
    public Circle node_r3c5_2;
    @FXML
    public Circle node_r3c6_1;
    @FXML
    public Circle node_r3c6_2;
    @FXML
    public Circle node_r3c7_1;
    @FXML
    public Circle node_r3c7_2;
    @FXML
    public Circle node_r3c8_1;
    @FXML
    public Circle node_r3c8_2;
    @FXML
    public Circle node_r3c9_1;
    @FXML
    public Circle node_r3c9_2;
    @FXML
    public Circle node_r3c10_1;
    @FXML
    public Circle node_r3c10_2;
    @FXML
    public Circle node_r3c11_1;
    @FXML
    public Circle node_r3c11_2;
    @FXML
    public Circle node_r3c12_1;
    @FXML
    public Circle node_r3c12_2;
    @FXML
    public Circle node_r3c13_1;
    @FXML
    public Circle node_r3c13_2;
    @FXML
    public Circle node_r3c14_1;
    @FXML
    public Circle node_r3c14_2;
    @FXML
    public Circle node_r3c15_1;
    @FXML
    public Circle node_r3c15_2;
    @FXML
    public Circle node_r4c0_1;
    @FXML
    public Circle node_r4c0_2;
    @FXML
    public Circle node_r4c1_1;
    @FXML
    public Circle node_r4c1_2;
    @FXML
    public Circle node_r4c2_1;
    @FXML
    public Circle node_r4c2_2;
    @FXML
    public Circle node_r4c3_1;
    @FXML
    public Circle node_r4c3_2;
    @FXML
    public Circle node_r4c4_1;
    @FXML
    public Circle node_r4c4_2;
    @FXML
    public Circle node_r4c5_1;
    @FXML
    public Circle node_r4c5_2;
    @FXML
    public Circle node_r4c6_1;
    @FXML
    public Circle node_r4c6_2;
    @FXML
    public Circle node_r4c7_1;
    @FXML
    public Circle node_r4c7_2;
    @FXML
    public Circle node_r4c8_1;
    @FXML
    public Circle node_r4c8_2;
    @FXML
    public Circle node_r4c9_1;
    @FXML
    public Circle node_r4c9_2;
    @FXML
    public Circle node_r4c10_1;
    @FXML
    public Circle node_r4c10_2;
    @FXML
    public Circle node_r4c11_1;
    @FXML
    public Circle node_r4c11_2;
    @FXML
    public Circle node_r4c12_1;
    @FXML
    public Circle node_r4c12_2;
    @FXML
    public Circle node_r4c13_1;
    @FXML
    public Circle node_r4c13_2;
    @FXML
    public Circle node_r4c14_1;
    @FXML
    public Circle node_r4c14_2;
    @FXML
    public Circle node_r4c15_1;
    @FXML
    public Circle node_r4c15_2;
    @FXML
    public Circle node_r5c0_1;
    @FXML
    public Circle node_r5c0_2;
    @FXML
    public Circle node_r5c1_1;
    @FXML
    public Circle node_r5c1_2;
    @FXML
    public Circle node_r5c2_1;
    @FXML
    public Circle node_r5c2_2;
    @FXML
    public Circle node_r5c3_1;
    @FXML
    public Circle node_r5c3_2;
    @FXML
    public Circle node_r5c4_1;
    @FXML
    public Circle node_r5c4_2;
    @FXML
    public Circle node_r5c5_1;
    @FXML
    public Circle node_r5c5_2;
    @FXML
    public Circle node_r5c6_1;
    @FXML
    public Circle node_r5c6_2;
    @FXML
    public Circle node_r5c7_1;
    @FXML
    public Circle node_r5c7_2;
    @FXML
    public Circle node_r5c8_1;
    @FXML
    public Circle node_r5c8_2;
    @FXML
    public Circle node_r5c9_1;
    @FXML
    public Circle node_r5c9_2;
    @FXML
    public Circle node_r5c10_1;
    @FXML
    public Circle node_r5c10_2;
    @FXML
    public Circle node_r5c11_1;
    @FXML
    public Circle node_r5c11_2;
    @FXML
    public Circle node_r5c12_1;
    @FXML
    public Circle node_r5c12_2;
    @FXML
    public Circle node_r5c13_1;
    @FXML
    public Circle node_r5c13_2;
    @FXML
    public Circle node_r5c14_1;
    @FXML
    public Circle node_r5c14_2;
    @FXML
    public Circle node_r5c15_1;
    @FXML
    public Circle node_r5c15_2;
    @FXML
    public Circle node_r6c0_1;
    @FXML
    public Circle node_r6c0_2;
    @FXML
    public Circle node_r6c1_1;
    @FXML
    public Circle node_r6c1_2;
    @FXML
    public Circle node_r6c2_1;
    @FXML
    public Circle node_r6c2_2;
    @FXML
    public Circle node_r6c3_1;
    @FXML
    public Circle node_r6c3_2;
    @FXML
    public Circle node_r6c4_1;
    @FXML
    public Circle node_r6c4_2;
    @FXML
    public Circle node_r6c5_1;
    @FXML
    public Circle node_r6c5_2;
    @FXML
    public Circle node_r6c6_1;
    @FXML
    public Circle node_r6c6_2;
    @FXML
    public Circle node_r6c7_1;
    @FXML
    public Circle node_r6c7_2;
    @FXML
    public Circle node_r6c8_1;
    @FXML
    public Circle node_r6c8_2;
    @FXML
    public Circle node_r6c9_1;
    @FXML
    public Circle node_r6c9_2;
    @FXML
    public Circle node_r6c10_1;
    @FXML
    public Circle node_r6c10_2;
    @FXML
    public Circle node_r6c11_1;
    @FXML
    public Circle node_r6c11_2;
    @FXML
    public Circle node_r6c12_1;
    @FXML
    public Circle node_r6c12_2;
    @FXML
    public Circle node_r6c13_1;
    @FXML
    public Circle node_r6c13_2;
    @FXML
    public Circle node_r6c14_1;
    @FXML
    public Circle node_r6c14_2;
    @FXML
    public Circle node_r6c15_1;
    @FXML
    public Circle node_r6c15_2;
    @FXML
    public Circle node_r7c0_1;
    @FXML
    public Circle node_r7c0_2;
    @FXML
    public Circle node_r7c1_1;
    @FXML
    public Circle node_r7c1_2;
    @FXML
    public Circle node_r7c2_1;
    @FXML
    public Circle node_r7c2_2;
    @FXML
    public Circle node_r7c3_1;
    @FXML
    public Circle node_r7c3_2;
    @FXML
    public Circle node_r7c4_1;
    @FXML
    public Circle node_r7c4_2;
    @FXML
    public Circle node_r7c5_1;
    @FXML
    public Circle node_r7c5_2;
    @FXML
    public Circle node_r7c6_1;
    @FXML
    public Circle node_r7c6_2;
    @FXML
    public Circle node_r7c7_1;
    @FXML
    public Circle node_r7c7_2;
    @FXML
    public Circle node_r7c8_1;
    @FXML
    public Circle node_r7c8_2;
    @FXML
    public Circle node_r7c9_1;
    @FXML
    public Circle node_r7c9_2;
    @FXML
    public Circle node_r7c10_1;
    @FXML
    public Circle node_r7c10_2;
    @FXML
    public Circle node_r7c11_1;
    @FXML
    public Circle node_r7c11_2;
    @FXML
    public Circle node_r7c12_1;
    @FXML
    public Circle node_r7c12_2;
    @FXML
    public Circle node_r7c13_1;
    @FXML
    public Circle node_r7c13_2;
    @FXML
    public Circle node_r7c14_1;
    @FXML
    public Circle node_r7c14_2;
    @FXML
    public Circle node_r7c15_1;
    @FXML
    public Circle node_r7c15_2;

    // Истина когда происходит редактирование программы
    private  boolean editing = false;
    //Объект текущей программы
    private ProgramObjectClass currProgram;
    //Создаем объекты реле
    Relay r_r0c0 = new Relay("Реле_r0c0", "b0r0c0");
    Relay r_r0c1 = new Relay("Реле_r0c1", "b0r0c1");
    Relay r_r0c2 = new Relay("Реле_r0c2", "b0r0c2");
    Relay r_r0c3 = new Relay("Реле_r0c3", "b0r0c3");
    Relay r_r0c4 = new Relay("Реле_r0c4", "b0r0c4");
    Relay r_r0c5 = new Relay("Реле_r0c5", "b0r0c5");
    Relay r_r0c6 = new Relay("Реле_r0c6", "b0r0c6");
    Relay r_r0c7 = new Relay("Реле_r0c7", "b0r0c7");
    Relay r_r0c8 = new Relay("Реле_r0c8", "b0r0c8");
    Relay r_r0c9 = new Relay("Реле_r0c9", "b0r0c9");
    Relay r_r0c10 = new Relay("Реле_r0c10", "b0r0c10");
    Relay r_r0c11 = new Relay("Реле_r0c11", "b0r0c11");
    Relay r_r0c12 = new Relay("Реле_r0c12", "b0r0c12");
    Relay r_r0c13 = new Relay("Реле_r0c13", "b0r0c13");
    Relay r_r0c14 = new Relay("Реле_r0c14", "b0r0c14");
    Relay r_r0c15 = new Relay("Реле_r0c15", "b0r0c15");
    Relay r_r1c0 = new Relay("Реле_r1c0", "b0r1c0");
    Relay r_r1c1 = new Relay("Реле_r1c1", "b0r1c1");
    Relay r_r1c2 = new Relay("Реле_r1c2", "b0r1c2");
    Relay r_r1c3 = new Relay("Реле_r1c3", "b0r1c3");
    Relay r_r1c4 = new Relay("Реле_r1c4", "b0r1c4");
    Relay r_r1c5 = new Relay("Реле_r1c5", "b0r1c5");
    Relay r_r1c6 = new Relay("Реле_r1c6", "b0r1c6");
    Relay r_r1c7 = new Relay("Реле_r1c7", "b0r1c7");
    Relay r_r1c8 = new Relay("Реле_r1c8", "b0r1c8");
    Relay r_r1c9 = new Relay("Реле_r1c9", "b0r1c9");
    Relay r_r1c10 = new Relay("Реле_r1c10", "b0r1c10");
    Relay r_r1c11 = new Relay("Реле_r1c11", "b0r1c11");
    Relay r_r1c12 = new Relay("Реле_r1c12", "b0r1c12");
    Relay r_r1c13 = new Relay("Реле_r1c13", "b0r1c13");
    Relay r_r1c14 = new Relay("Реле_r1c14", "b0r1c14");
    Relay r_r1c15 = new Relay("Реле_r1c15", "b0r1c15");
    Relay r_r2c0 = new Relay("Реле_r2c0", "b0r2c0");
    Relay r_r2c1 = new Relay("Реле_r2c1", "b0r2c1");
    Relay r_r2c2 = new Relay("Реле_r2c2", "b0r2c2");
    Relay r_r2c3 = new Relay("Реле_r2c3", "b0r2c3");
    Relay r_r2c4 = new Relay("Реле_r2c4", "b0r2c4");
    Relay r_r2c5 = new Relay("Реле_r2c5", "b0r2c5");
    Relay r_r2c6 = new Relay("Реле_r2c6", "b0r2c6");
    Relay r_r2c7 = new Relay("Реле_r2c7", "b0r2c7");
    Relay r_r2c8 = new Relay("Реле_r2c8", "b0r2c8");
    Relay r_r2c9 = new Relay("Реле_r2c9", "b0r2c9");
    Relay r_r2c10 = new Relay("Реле_r2c10", "b0r2c10");
    Relay r_r2c11 = new Relay("Реле_r2c11", "b0r2c11");
    Relay r_r2c12 = new Relay("Реле_r2c12", "b0r2c12");
    Relay r_r2c13 = new Relay("Реле_r2c13", "b0r2c13");
    Relay r_r2c14 = new Relay("Реле_r2c14", "b0r2c14");
    Relay r_r2c15 = new Relay("Реле_r2c15", "b0r2c15");
    Relay r_r3c0 = new Relay("Реле_r3c0", "b0r3c0");
    Relay r_r3c1 = new Relay("Реле_r3c1", "b0r3c1");
    Relay r_r3c2 = new Relay("Реле_r3c2", "b0r3c2");
    Relay r_r3c3 = new Relay("Реле_r3c3", "b0r3c3");
    Relay r_r3c4 = new Relay("Реле_r3c4", "b0r3c4");
    Relay r_r3c5 = new Relay("Реле_r3c5", "b0r3c5");
    Relay r_r3c6 = new Relay("Реле_r3c6", "b0r3c6");
    Relay r_r3c7 = new Relay("Реле_r3c7", "b0r3c7");
    Relay r_r3c8 = new Relay("Реле_r3c8", "b0r3c8");
    Relay r_r3c9 = new Relay("Реле_r3c9", "b0r3c9");
    Relay r_r3c10 = new Relay("Реле_r3c10", "b0r3c10");
    Relay r_r3c11 = new Relay("Реле_r3c11", "b0r3c11");
    Relay r_r3c12 = new Relay("Реле_r3c12", "b0r3c12");
    Relay r_r3c13 = new Relay("Реле_r3c13", "b0r3c13");
    Relay r_r3c14 = new Relay("Реле_r3c14", "b0r3c14");
    Relay r_r3c15 = new Relay("Реле_r3c15", "b0r3c15");
    Relay r_r4c0 = new Relay("Реле_r4c0", "b1r0c0");
    Relay r_r4c1 = new Relay("Реле_r4c1", "b1r0c1");
    Relay r_r4c2 = new Relay("Реле_r4c2", "b1r0c2");
    Relay r_r4c3 = new Relay("Реле_r4c3", "b1r0c3");
    Relay r_r4c4 = new Relay("Реле_r4c4", "b1r0c4");
    Relay r_r4c5 = new Relay("Реле_r4c5", "b1r0c5");
    Relay r_r4c6 = new Relay("Реле_r4c6", "b1r0c6");
    Relay r_r4c7 = new Relay("Реле_r4c7", "b1r0c7");
    Relay r_r4c8 = new Relay("Реле_r4c8", "b1r0c8");
    Relay r_r4c9 = new Relay("Реле_r4c9", "b1r0c9");
    Relay r_r4c10 = new Relay("Реле_r4c10", "b1r0c10");
    Relay r_r4c11 = new Relay("Реле_r4c11", "b1r0c11");
    Relay r_r4c12 = new Relay("Реле_r4c12", "b1r0c12");
    Relay r_r4c13 = new Relay("Реле_r4c13", "b1r0c13");
    Relay r_r4c14 = new Relay("Реле_r4c14", "b1r0c14");
    Relay r_r4c15 = new Relay("Реле_r4c15", "b1r0c15");
    Relay r_r5c0 = new Relay("Реле_r5c0", "b1r1c0");
    Relay r_r5c1 = new Relay("Реле_r5c1", "b1r1c1");
    Relay r_r5c2 = new Relay("Реле_r5c2", "b1r1c2");
    Relay r_r5c3 = new Relay("Реле_r5c3", "b1r1c3");
    Relay r_r5c4 = new Relay("Реле_r5c4", "b1r1c4");
    Relay r_r5c5 = new Relay("Реле_r5c5", "b1r1c5");
    Relay r_r5c6 = new Relay("Реле_r5c6", "b1r1c6");
    Relay r_r5c7 = new Relay("Реле_r5c7", "b1r1c7");
    Relay r_r5c8 = new Relay("Реле_r5c8", "b1r1c8");
    Relay r_r5c9 = new Relay("Реле_r5c9", "b1r1c9");
    Relay r_r5c10 = new Relay("Реле_r5c10", "b1r1c10");
    Relay r_r5c11 = new Relay("Реле_r5c11", "b1r1c11");
    Relay r_r5c12 = new Relay("Реле_r5c12", "b1r1c12");
    Relay r_r5c13 = new Relay("Реле_r5c13", "b1r1c13");
    Relay r_r5c14 = new Relay("Реле_r5c14", "b1r1c14");
    Relay r_r5c15 = new Relay("Реле_r5c15", "b1r1c15");
    Relay r_r6c0 = new Relay("Реле_r6c0", "b1r2c0");
    Relay r_r6c1 = new Relay("Реле_r6c1", "b1r2c1");
    Relay r_r6c2 = new Relay("Реле_r6c2", "b1r2c2");
    Relay r_r6c3 = new Relay("Реле_r6c3", "b1r2c3");
    Relay r_r6c4 = new Relay("Реле_r6c4", "b1r2c4");
    Relay r_r6c5 = new Relay("Реле_r6c5", "b1r2c5");
    Relay r_r6c6 = new Relay("Реле_r6c6", "b1r2c6");
    Relay r_r6c7 = new Relay("Реле_r6c7", "b1r2c7");
    Relay r_r6c8 = new Relay("Реле_r6c8", "b1r2c8");
    Relay r_r6c9 = new Relay("Реле_r6c9", "b1r2c9");
    Relay r_r6c10 = new Relay("Реле_r6c10", "b1r2c10");
    Relay r_r6c11 = new Relay("Реле_r6c11", "b1r2c11");
    Relay r_r6c12 = new Relay("Реле_r6c12", "b1r2c12");
    Relay r_r6c13 = new Relay("Реле_r6c13", "b1r2c13");
    Relay r_r6c14 = new Relay("Реле_r6c14", "b1r2c14");
    Relay r_r6c15 = new Relay("Реле_r6c15", "b1r2c15");
    Relay r_r7c0 = new Relay("Реле_r7c0", "b1r3c0");
    Relay r_r7c1 = new Relay("Реле_r7c1", "b1r3c1");
    Relay r_r7c2 = new Relay("Реле_r7c2", "b1r3c2");
    Relay r_r7c3 = new Relay("Реле_r7c3", "b1r3c3");
    Relay r_r7c4 = new Relay("Реле_r7c4", "b1r3c4");
    Relay r_r7c5 = new Relay("Реле_r7c5", "b1r3c5");
    Relay r_r7c6 = new Relay("Реле_r7c6", "b1r3c6");
    Relay r_r7c7 = new Relay("Реле_r7c7", "b1r3c7");
    Relay r_r7c8 = new Relay("Реле_r7c8", "b1r3c8");
    Relay r_r7c9 = new Relay("Реле_r7c9", "b1r3c9");
    Relay r_r7c10 = new Relay("Реле_r7c10", "b1r3c10");
    Relay r_r7c11 = new Relay("Реле_r7c11", "b1r3c11");
    Relay r_r7c12 = new Relay("Реле_r7c12", "b1r3c12");
    Relay r_r7c13 = new Relay("Реле_r7c13", "b1r3c13");
    Relay r_r7c14 = new Relay("Реле_r7c14", "b1r3c14");
    Relay r_r7c15 = new Relay("Реле_r7c15", "b1r3c15");
    //Создаем объекты источников/измерителей
    PowerSource I1 = new PowerSource("G1", "PXIe-4139",60.0F,3.0F);
    PowerSource I2 = new PowerSource("G2", "PXIe-4137",200.0F,1.0F);
    PowerSource I3 = new PowerSource("G3", "PXIe-4137",200.0F,1.0F);
    PowerSource I4 = new PowerSource("G4", "PXIe-4137",200.0F,1.0F);
    //Инициализауия рабочей папки
    String filePath = "";
    private Map<String, Map<String, Object>> objects;
    private Logger Log;
    //Глобальные переменные дерева !!!! - временно, позже убрать, сделать перерисовку дерева отдельно,
    //работа с объектом класса ProgramObjectClass

    void initNewProgram(){

        Log.info("Создаем новую программу");
        // Дерево проекта программы - определяем корневой узел
        TreeItem<String> rootTreeNode = new TreeItem<String>("Программа");
        // устанавливаем корневой узел для TreeView
        TreeView<String> struct_tree = new TreeView<String>(rootTreeNode);
        treeFlowPane.setMaxSize(365, 750);
        //очистить дерево!!
        treeFlowPane.getChildren().clear();
        this.currProgram = new ProgramObjectClass();
        currProgram.setMainController(this);
        currProgram.setLogger(Log);
        try {
            currProgram.setProgramName(twoStateDialogWithTextField("Название программы:","   ", "string"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        //передача карты в обработчик текста программы
        this.currProgram.setGUIObjects(objects);

        //currProgram.setProgramName("Тестер");
        treeFlowPane.getChildren().add(currProgram.createNewProgram());
        setListenerForCurrProgramTreeView();
        treeFlowPane.autosize();
        currProgram.setCurrentInputData(currentDataIn);

        workPane.setVisible(false);
    }
    void openProgramFile(){
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().addAll(//
                new FileChooser.ExtensionFilter("XML", "*.xml"),
                new FileChooser.ExtensionFilter("All Files", "*.*"));
        chooser.setInitialDirectory(new File(PathHolder.getProgramPath()));

        File dir = chooser.showOpenDialog((Stage) menu_buttons_bar.getScene().getWindow());
        if (dir != null) {
            filePath = dir.getAbsolutePath();
            this.currProgram = new ProgramObjectClass();
            currProgram.setMainController(this);
            currProgram.setLogger(Log);
            currProgram.setFilePath(filePath);
            PathHolder.setProgramPath(new File(filePath).getParent());
            //передача карты в обработчик текста программы
            this.currProgram.setGUIObjects(objects);
            treeFlowPane.getChildren().clear();
            treeFlowPane.getChildren().add(currProgram.readProgramFile());
            if (this.currProgram.getBlockCount()>0) {
                workPane.setVisible(true);
            }
            setListenerForCurrProgramTreeView();
            currentDataIn = currProgram.getProgramDataIn();
        }
        //workPane.setVisible(true);
    }

    private void setListenerForCurrProgramTreeView() {
        /**
         * Функция индексирует путь до выделенного элемента
         * затем передает его в объект программы для поиска выделенного обекта
         * в самое программе*/
        currProgram.getStruct_tree().getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue,
                                Object newValue) {
                try {
                    TreeItem<String> selectedItem = (TreeItem<String>) newValue;
                    if (selectedItem == null) return;
                    ArrayList<Integer> indexMap = Methods.getIndexMapForSelectedTreeItem(selectedItem);
                    Log.info(currProgram.setSelectedNodeByIndexMapAndReturnByString(indexMap));
                }catch (Exception e){
                    Log.info("Main: setListenerForCurrProgramTreeView Error: "+e.fillInStackTrace());
                }
            }

        });

        currProgram.getStruct_tree().setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent mouseEvent)
            {
                if(mouseEvent.getClickCount() == 2)
                {
                    currProgram.editSelectedNode();
                }
            }
        });

    }

    boolean twoStateDialog(String message) throws IOException {
        Stage dialog = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("views/yes_or_no.fxml"));
        Parent root = loader.load();


        dialog.setTitle(message);
        dialog.setScene(new Scene(root, 350, 200));
        // populate dialog with controls.

        //dialog.initOwner(parentStage);
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog_controller controller = loader.<dialog_controller>getController();
        controller.setMessage(message);
        dialog.showAndWait();
        //получаем статус завершения диалога с пользователем
        return (controller.getDialogStatus() == DialogEndStatus.YES?true:false);
    }

    String twoStateDialogWithTextField(String message, String textFieldLabel,  String inputType) throws IOException {
        Stage dialog = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("views/YN_with_textIn.fxml"));
        Parent root = loader.load();


        dialog.setTitle(message);
        dialog.setScene(new Scene(root, 350, 200));
        // populate dialog with controls.

        //dialog.initOwner(parentStage);
        dialog.initModality(Modality.APPLICATION_MODAL);
        YN_with_textIn_controller controller = loader.<YN_with_textIn_controller>getController();
        controller.setContent(message, textFieldLabel, inputType);
        dialog.showAndWait();
        //получаем статус завершения диалога с пользователем
        Log.info(controller.getInputText());
        return (controller.getInputText());
    }

    private void tryInitNewProgram(){

        //сделать видимыми поля схемы и прочее
        if (!editing) {
            editing = true;
            initNewProgram();
        } else {
            //спросить про удалить или сохранить текущую прогу
            Log.info("удалить или сохранить текущую прогу?");
            try {
                if (currProgram!=null){
                    if (currProgram.ProgramSaved?false:twoStateDialog("Сохранить текущую программу?")) {
                        //алгоритм сохранения текущей прогрраммы
                        if (currProgram.SaveProgramFile())
                            initNewProgram();
                    } else {
                        initNewProgram();
                    }
                }else {
                    initNewProgram();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void tryOpenProgram(){
        if (!editing) {
            openProgramFile();
            editing = currProgram!=null;
        } else {
            Log.info("удалить или сохранить текущую прогу?");
            try {
                if (currProgram.ProgramSaved?false:twoStateDialog("Сохранить текущую программу?")) {
                    //алгоритм сохранения текущей прогрраммы
                    if (currProgram.SaveProgramFile())
                        openProgramFile();
                } else {
                    openProgramFile();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void tryRunCurrProgram(){
        Log.info("Try to run this program now!");
        if (!currProgram.ProgramSaved){
            try {
                if (Methods.twoStateDialog("Сохранить программу перед запуском?") == DialogEndStatus.YES){
                    currProgram.SaveProgramFile();
                }
            } catch (IOException e) {
                Log.info("Try to run program error"+e.getLocalizedMessage());
            }
        }
        int filenameStartIndex = currProgram.getFilePath().lastIndexOf('\\') + 1;
        String libPath = currProgram.getFilePath().substring(0, filenameStartIndex-1);
        String filePath = currProgram.getFilePath().substring(filenameStartIndex);
        try {
            Methods.runExecPrgController(libPath, filePath, Log);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void tryRunProgram(){
        Log.info("Try to run any programs now!");
        if (currProgram!=null && editing) {
            if (!currProgram.ProgramSaved) {
                try {
                    if (Methods.twoStateDialog("Сохранить программу перед запуском?") == DialogEndStatus.YES) {
                        currProgram.SaveProgramFile();
                    }
                } catch (IOException e) {
                    Log.info("Try to save program before run program error" + e.getLocalizedMessage());
                }
            }
        }
        try {
            Methods.runExecPrgController(Log);
        } catch (IOException e) {
            Log.info("Try to run programs error" + e.getStackTrace().toString());
        }
    }

    @FXML
    void initialize() {
        //Setting objects
        objectsSetting();
        //Настройка меню
        Menu fileMenu = new Menu("Файл");
        fileMenu.getItems().addAll(
                new MenuItem("Создать"),
                new MenuItem("Открыть"),
                new MenuItem("Сохранить"),
                new MenuItem("Сохранить как"));
        fileMenu.setId("#fileMenu");
        Menu editMenu = new Menu("Правка");
        editMenu.getItems().addAll(
                new MenuItem("Отменить последнее действие"),
                new MenuItem("Убрать границы запуска"),
                new MenuItem("Работа с библиотеками"),
                new MenuItem("Задать путь к библиотеке"));
        editMenu.setId("#editMenu");
        Menu runMenu = new Menu("Запуск");
        runMenu.getItems().addAll(
                new MenuItem("Запустить текущую программу"),
                new MenuItem("Расширенный запуск"));
        runMenu.setId("#runMenu");
        menu_buttons_bar.getMenus().clear();
        menu_buttons_bar.getMenus().addAll(
                fileMenu,
                editMenu,
                runMenu
        );
        //сделать невидимой рабочую область (со схемой)
        workPane.setVisible(false);

        //Обработка создания новой программы
        fileMenu.getItems().get(0).setOnAction(event -> {
            tryInitNewProgram();
        });

        //Файл
        //отработка нажатия кнопки ОТКРЫТЬ в меню файл
        fileMenu.getItems().get(1).setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                tryOpenProgram();
            }
        });
        //Сохранить
        fileMenu.getItems().get(2).setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (editing) {
                    currProgram.SaveProgramFile();
                }
            }
        });
        //Short cats
        workPane.getParent().getParent().setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.S && event.isControlDown() && editing) {
                    currProgram.SaveProgramFile();
                }
                if (event.getCode() == KeyCode.Z && event.isControlDown() && editing) {
                    currProgram.delOneChange();
                }
                if (event.getCode() == KeyCode.DELETE && editing) {
                    try {
                        if(Methods.twoStateDialog("Удалить выделенные элементы?") == DialogEndStatus.YES){
                            currProgram.delSelectedNodes();
                        }
                    } catch (IOException e) {
                        Log.info("delSelectedNode by DELETE keyBoard button IO error"+e.getStackTrace().toString());
                    }
                }
                if (event.getCode() == KeyCode.N && event.isControlDown()) {
                    tryInitNewProgram();
                }
                if (event.getCode() == KeyCode.O && event.isControlDown()) {
                    tryOpenProgram();
                }
            }
        });
        //Сохранить как
        fileMenu.getItems().get(3).setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (editing) {
                    currProgram.SaveAsProgramFile();
                }
            }
        });
        //правка
        //отменить
        editMenu.getItems().get(0).setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (editing) {
                    currProgram.delOneChange();
                }
            }
        });
        editMenu.getItems().get(1).setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (editing) {
                    currProgram.resetExecutingBounds();
                }
            }
        });
        editMenu.getItems().get(2).setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (editing) {
                    currProgram.getLibrary();
                }else{
                    try {
                        Methods.messageDialog("Предварительно создайте новую программу!");
                    } catch (IOException e) {
                        Log.info(e.getStackTrace().toString());
                    }
                }
            }
        });
        editMenu.getItems().get(3).setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (editing) {
                    currProgram.selectLibPath();
                }else{
                    try {
                        Methods.messageDialog("Предварительно создайте новую программу!");
                    } catch (IOException e) {
                        Log.info(e.getStackTrace().toString());
                    }
                }
            }
        });
        //запуск
        runMenu.getItems().get(0).setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (editing) {
                    tryRunCurrProgram();
                }else{
                    try {
                        Methods.messageDialog("Предварительно откройте программу!");
                    } catch (IOException e) {
                        Log.info(e.getStackTrace().toString());
                    }
                }
            }
        });
        runMenu.getItems().get(1).setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                    tryRunProgram();
            }
        });
        //обработка кнопки ввод на панели
        dataInputButt.setOnAction(event -> {
            try{
                Stage dialog = new Stage();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("views/dataInput.fxml"));
                Parent root = loader.load();
                dialog.setScene(new Scene(root));
                dialog.initModality(Modality.APPLICATION_MODAL);
                dataInputController controller = loader.<dataInputController>getController();
                controller.sendPreviousData(currentDataIn);
                dialog.showAndWait();
                if(controller.getDialog_status().equals("yes")) {
                    currentDataIn = (controller.getDataIn());
                    if (editing){
                        currProgram.setCurrentInputData(currentDataIn);
                    }
                }
            }catch (IOException e){
                Log.info("GUI Error dataInputButt"+e.getLocalizedMessage());
            }
        });
        //обработка кнопки результат
        resultButt.setOnAction(event -> {
            if (currProgram.getMeasCount()>0){
                try{
                    Stage dialog = new Stage();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("views/result.fxml"));
                    Parent root = loader.load();
                    dialog.setScene(new Scene(root));
                    dialog.initModality(Modality.APPLICATION_MODAL);
                    ResultController controller = loader.<ResultController>getController();
                    controller.sendMeasListData(currProgram.getMeasNodes());
                    dialog.showAndWait();
                    if(controller.getDialog_status()==DialogEndStatus.YES) {
                        currProgram.addResultBlock(controller.getOutValues());
                    }
                }catch (IOException e){
                    Log.info("GUI Error dataInputButt"+e.getLocalizedMessage());
                }
            }else{
                try {
                    Methods.messageDialog("В блоке нет измерений!");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        //отработка простоения ВАХ
        createVACButt.setOnAction(event -> {
            if(editing && currProgram.getBlockCount()>0) {
                try {
                    Stage dialog = new Stage();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("views/VAC.fxml"));
                    Parent root = loader.load();
                    dialog.setScene(new Scene(root));
                    dialog.initModality(Modality.APPLICATION_MODAL);
                    VACController controller = loader.<VACController>getController();
                    controller.sendSources(objects.get("powerSources").keySet().toArray());
                    dialog.showAndWait();
                    if (controller.getDialog_status().equals("yes"))
                        currProgram.addVACBlock(controller.getOutValues(), true);
                } catch (IOException e) {
                    Log.info("GUI Error VACBlock" + e.getLocalizedMessage());
                }
            }
        });
        //Отработка узлов коммутаци
        try {
            relayOnMouseClicked(r_r0c0);
            relayOnMouseClicked(r_r0c1);
            relayOnMouseClicked(r_r0c2);
            relayOnMouseClicked(r_r0c3);
            relayOnMouseClicked(r_r0c4);
            relayOnMouseClicked(r_r0c5);
            relayOnMouseClicked(r_r0c6);
            relayOnMouseClicked(r_r0c7);
            relayOnMouseClicked(r_r0c8);
            relayOnMouseClicked(r_r0c9);
            relayOnMouseClicked(r_r0c10);
            relayOnMouseClicked(r_r0c11);
            relayOnMouseClicked(r_r0c12);
            relayOnMouseClicked(r_r0c13);
            relayOnMouseClicked(r_r0c14);
            relayOnMouseClicked(r_r0c15);
            relayOnMouseClicked(r_r1c0);
            relayOnMouseClicked(r_r1c1);
            relayOnMouseClicked(r_r1c2);
            relayOnMouseClicked(r_r1c3);
            relayOnMouseClicked(r_r1c4);
            relayOnMouseClicked(r_r1c5);
            relayOnMouseClicked(r_r1c6);
            relayOnMouseClicked(r_r1c7);
            relayOnMouseClicked(r_r1c8);
            relayOnMouseClicked(r_r1c9);
            relayOnMouseClicked(r_r1c10);
            relayOnMouseClicked(r_r1c11);
            relayOnMouseClicked(r_r1c12);
            relayOnMouseClicked(r_r1c13);
            relayOnMouseClicked(r_r1c14);
            relayOnMouseClicked(r_r1c15);
            relayOnMouseClicked(r_r2c0);
            relayOnMouseClicked(r_r2c1);
            relayOnMouseClicked(r_r2c2);
            relayOnMouseClicked(r_r2c3);
            relayOnMouseClicked(r_r2c4);
            relayOnMouseClicked(r_r2c5);
            relayOnMouseClicked(r_r2c6);
            relayOnMouseClicked(r_r2c7);
            relayOnMouseClicked(r_r2c8);
            relayOnMouseClicked(r_r2c9);
            relayOnMouseClicked(r_r2c10);
            relayOnMouseClicked(r_r2c11);
            relayOnMouseClicked(r_r2c12);
            relayOnMouseClicked(r_r2c13);
            relayOnMouseClicked(r_r2c14);
            relayOnMouseClicked(r_r2c15);
            relayOnMouseClicked(r_r3c0);
            relayOnMouseClicked(r_r3c1);
            relayOnMouseClicked(r_r3c2);
            relayOnMouseClicked(r_r3c3);
            relayOnMouseClicked(r_r3c4);
            relayOnMouseClicked(r_r3c5);
            relayOnMouseClicked(r_r3c6);
            relayOnMouseClicked(r_r3c7);
            relayOnMouseClicked(r_r3c8);
            relayOnMouseClicked(r_r3c9);
            relayOnMouseClicked(r_r3c10);
            relayOnMouseClicked(r_r3c11);
            relayOnMouseClicked(r_r3c12);
            relayOnMouseClicked(r_r3c13);
            relayOnMouseClicked(r_r3c14);
            relayOnMouseClicked(r_r3c15);
            relayOnMouseClicked(r_r4c0);
            relayOnMouseClicked(r_r4c1);
            relayOnMouseClicked(r_r4c2);
            relayOnMouseClicked(r_r4c3);
            relayOnMouseClicked(r_r4c4);
            relayOnMouseClicked(r_r4c5);
            relayOnMouseClicked(r_r4c6);
            relayOnMouseClicked(r_r4c7);
            relayOnMouseClicked(r_r4c8);
            relayOnMouseClicked(r_r4c9);
            relayOnMouseClicked(r_r4c10);
            relayOnMouseClicked(r_r4c11);
            relayOnMouseClicked(r_r4c12);
            relayOnMouseClicked(r_r4c13);
            relayOnMouseClicked(r_r4c14);
            relayOnMouseClicked(r_r4c15);
            relayOnMouseClicked(r_r5c0);
            relayOnMouseClicked(r_r5c1);
            relayOnMouseClicked(r_r5c2);
            relayOnMouseClicked(r_r5c3);
            relayOnMouseClicked(r_r5c4);
            relayOnMouseClicked(r_r5c5);
            relayOnMouseClicked(r_r5c6);
            relayOnMouseClicked(r_r5c7);
            relayOnMouseClicked(r_r5c8);
            relayOnMouseClicked(r_r5c9);
            relayOnMouseClicked(r_r5c10);
            relayOnMouseClicked(r_r5c11);
            relayOnMouseClicked(r_r5c12);
            relayOnMouseClicked(r_r5c13);
            relayOnMouseClicked(r_r5c14);
            relayOnMouseClicked(r_r5c15);
            relayOnMouseClicked(r_r6c0);
            relayOnMouseClicked(r_r6c1);
            relayOnMouseClicked(r_r6c2);
            relayOnMouseClicked(r_r6c3);
            relayOnMouseClicked(r_r6c4);
            relayOnMouseClicked(r_r6c5);
            relayOnMouseClicked(r_r6c6);
            relayOnMouseClicked(r_r6c7);
            relayOnMouseClicked(r_r6c8);
            relayOnMouseClicked(r_r6c9);
            relayOnMouseClicked(r_r6c10);
            relayOnMouseClicked(r_r6c11);
            relayOnMouseClicked(r_r6c12);
            relayOnMouseClicked(r_r6c13);
            relayOnMouseClicked(r_r6c14);
            relayOnMouseClicked(r_r6c15);
            relayOnMouseClicked(r_r7c0);
            relayOnMouseClicked(r_r7c1);
            relayOnMouseClicked(r_r7c2);
            relayOnMouseClicked(r_r7c3);
            relayOnMouseClicked(r_r7c4);
            relayOnMouseClicked(r_r7c5);
            relayOnMouseClicked(r_r7c6);
            relayOnMouseClicked(r_r7c7);
            relayOnMouseClicked(r_r7c8);
            relayOnMouseClicked(r_r7c9);
            relayOnMouseClicked(r_r7c10);
            relayOnMouseClicked(r_r7c11);
            relayOnMouseClicked(r_r7c12);
            relayOnMouseClicked(r_r7c13);
            relayOnMouseClicked(r_r7c14);
            relayOnMouseClicked(r_r7c15);

        }catch (Exception e){Log.info("GUI Relays Error: "+ e.fillInStackTrace());}
        //обработка нажатия на источники
        voltageSourceOnMouseClicked(I1);
        voltageMeasOnMouseClicked(I1);
        currentSourceOnMouseClicked(I1);
        currentMeasOnMouseClicked(I1);
        voltageSourceOnMouseClicked(I2);
        voltageMeasOnMouseClicked(I2);
        currentSourceOnMouseClicked(I2);
        currentMeasOnMouseClicked(I2);
        voltageSourceOnMouseClicked(I3);
        voltageMeasOnMouseClicked(I3);
        currentSourceOnMouseClicked(I3);
        currentMeasOnMouseClicked(I3);
        voltageSourceOnMouseClicked(I4);
        voltageMeasOnMouseClicked(I4);
        currentSourceOnMouseClicked(I4);
        currentMeasOnMouseClicked(I4);
        //для DAQ
        DAQ_img.setOnMouseClicked(event -> {
            try {
                //Methods.messageDialog("Це ж ДАК, твою мартышку!");
                Methods.messageDialog("Функционал данного блока находится на стадии разработки");
                /*Stage dialog = new Stage();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("DAQform.fxml"));
                Parent root = loader.load();

                dialog.setScene(new Scene(root));

                dialog.initModality(Modality.APPLICATION_MODAL);
                DAQController controller = loader.<DAQController>getController();
                dialog.showAndWait();
                if(controller.getDialog_status().equals("yes")){
                    //currProgram.addDoubleBlock(controller.getBlockName(),controller.getPrototypeBlockId());
                }*/
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        //Создание нового блока
        blockButt.setOnAction(event -> {
            if (editing){
                try{
                    String blockName = twoStateDialogWithTextField("Создать блок", "Имя блока:", "string");
                    if (blockName.length()>0){
                        currProgram.addBlock(blockName);
                        workPane.setVisible(true);
                    }
                }catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        //Создание нового параметра
        paramButt.setOnAction(event -> {
            if (editing){
                try{
                    String blockName = twoStateDialogWithTextField("Создать параметр", "Имя параметра:", "string");
                    if (blockName.length()>0){
                        currProgram.addParameter(blockName);
                        workPane.setVisible(true);
                    }
                }catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        //Создание нового параметра
        pauseButt.setOnAction(event -> {
            if (editing){
                try{
                    String pause = twoStateDialogWithTextField("Пауза", "Пауза, мс", "int");
                    if (pause.length()>0){
                        currProgram.addPause(pause);
                    }
                }catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        //Создание нового параметра
        allResetButt.setOnAction(event -> {
            if (editing && currProgram.getBlockCount()>0){
                currProgram.addFullReset();
            }
        });
        doubleBlockButt.setOnAction(event -> {
            if(editing && currProgram.getBlockCount()>0){
                try {
                    Stage dialog = new Stage();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("views/doubleBlock.fxml"));
                    Parent root = loader.load();

                    dialog.setScene(new Scene(root));

                    dialog.initModality(Modality.APPLICATION_MODAL);
                    doubleBlockController controller = loader.<doubleBlockController>getController();
                    controller.sendBlockNodes(currProgram.getProgramDocument().getElementsByTagName("block"));
                    dialog.showAndWait();
                    if(controller.getDialog_status().equals("yes"))
                        currProgram.addDoubleBlock(controller.getBlockName(),controller.getPrototypeBlockId());
                }catch (IOException e){
                    Log.info("GUI Error doubleBock"+e.getLocalizedMessage());
                }
            }
        });
        movingButt.setOnAction(event -> {
            if(editing){
                try {
                    Stage dialog = new Stage();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("views/moving.fxml"));
                    Parent root = loader.load();

                    dialog.setScene(new Scene(root));

                    dialog.initModality(Modality.APPLICATION_MODAL);
                    MovingController controller = loader.<MovingController>getController();
                    dialog.showAndWait();
                    Log.info(controller.getMoving());
                    if(controller.getDialog_status().equals("yes"))
                        currProgram.addMoving(controller.getMoving());
                }catch (IOException e){
                    Log.info("GUI Error moving"+e.getLocalizedMessage());
                }
            }
        });
        headerButt.setOnAction(event -> {
            if (editing && this.currProgram.getBlockCount()>0){
                try{
                    String blockName = twoStateDialogWithTextField("Заголовок", "Текст:", "string");
                    if (blockName.length()>0){
                        currProgram.addHeader(blockName);
                    }
                }catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        clearAllButt.setOnAction(event -> {
            try {
                if (Methods.twoStateDialog("Удалить все узлы?") == DialogEndStatus.YES){
                    currProgram.delAllNodes();
                }
            } catch (IOException e) {
                Log.info("Main: ClearAllButt Error"+e.fillInStackTrace());
            }
        });
        deleteItemButt.setOnAction(event -> {
            currProgram.delSelectedNodes();
        });
    }
    private void relayOnMouseClicked(Relay r){
        for (Circle node:r.getNodes()){
            node.setOnMouseClicked(event -> {
                r.change_state();
                currProgram.addRelay(r);
            });
            node.setOnMouseEntered(event ->{
                for(Circle n:r.getNodes()){
                    n.setRadius(n.getRadius()*1.2);
                }
            });
            node.setOnMouseExited(event ->{
                for(Circle n:r.getNodes()){
                    n.setRadius(n.getRadius()/1.2);
                }
            });
        }
    }
    private void voltageSourceOnMouseClicked(PowerSource I){
        I.VSourceIcon.setOnAction(event -> {
            try {
                Stage dialog = new Stage();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("views/voltSource.fxml"));
                Parent root = loader.load();

                dialog.setScene(new Scene(root));

                dialog.initModality(Modality.APPLICATION_MODAL);
                voltSourceController controller = loader.<voltSourceController>getController();
                controller.setSource(I);
                dialog.showAndWait();
                //получаем статус завершения диалога с пользователем
                if(controller.getDialog_status().equals("yes"))
                    currProgram.addVoltageSource(I.sourceId, controller.getValues());
            }catch (IOException e){
                Log.info("GUI Error "+I.sourceId+": "+e.fillInStackTrace());
            }
        });
    }
    private void voltageMeasOnMouseClicked(PowerSource I){
        I.VMeasIcon.setOnAction(event -> {
            try {
                Stage dialog = new Stage();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("views/measVolt.fxml"));
                Parent root = loader.load();

                dialog.setScene(new Scene(root));

                dialog.initModality(Modality.APPLICATION_MODAL);
                voltMeasController controller = loader.<voltMeasController>getController();
                controller.setSource(I);
                dialog.showAndWait();
                //получаем статус завершения диалога с пользователем
                if(controller.getDialog_status().equals("yes"))
                    currProgram.addVoltageMeas(I.sourceId, controller.getValues());
            }catch (IOException e){
                Log.info("GUI Error "+I.sourceId+": "+e.fillInStackTrace());
            }
        });
    }
    private void currentMeasOnMouseClicked(PowerSource I){
        I.IMeasIcon.setOnAction(event -> {
            try {
                Stage dialog = new Stage();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("views/measCurr.fxml"));
                Parent root = loader.load();

                dialog.setScene(new Scene(root));

                dialog.initModality(Modality.APPLICATION_MODAL);
                currMeasController controller = loader.<currMeasController>getController();
                controller.setSource(I);
                dialog.showAndWait();
                //получаем статус завершения диалога с пользователем
                if(controller.getDialog_status().equals("yes"))
                    currProgram.addCurrentMeas(I.sourceId, controller.getValues());
            }catch (IOException e){
                Log.info("GUI Error "+I.sourceId+": "+e.fillInStackTrace());
            }
        });
    }
    private void currentSourceOnMouseClicked(PowerSource I){
        I.ISourceIcon.setOnAction(event -> {
            try {
                Stage dialog = new Stage();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("views/currSource.fxml"));
                Parent root = loader.load();

                dialog.setScene(new Scene(root));

                dialog.initModality(Modality.APPLICATION_MODAL);
                currSourceController controller = loader.<currSourceController>getController();
                controller.setSource(I);
                dialog.showAndWait();
                //получаем статус завершения диалога с пользователем
                if(controller.getDialog_status().equals("yes"))
                    currProgram.addCurrentSource(I.sourceId, controller.getValues());
            }catch (IOException e){
                Log.info("GUI Error "+I.sourceId+": "+e.fillInStackTrace());
            }
        });
    }
    private void objectsSetting() {
        //карта объектов отображения
        this.objects = new HashMap<String, Map<String, Object>>();
        //Добавление панелей интерфейса
        Map<String, Object> panelsMap = new HashMap<String, Object>();
        objects.put("panels", panelsMap);
        panelsMap.put("workPane", workPane);
        panelsMap.put("indicateName", indicateNameLabel);
        panelsMap.put("indicateStatus", indicateStatusLabel);
        //Настройка объектов реле и добавление в карту
        Map<String, Object> relaysMap = new HashMap<String, Object>();
        objects.put("relays", relaysMap);
        r_r0c0.setNodes(new Circle[] {node_r0c0_1,node_r0c0_2});
        relaysMap.put(r_r0c0.getRelay_id(),r_r0c0);
        r_r0c1.setNodes(new Circle[] {node_r0c1_1,node_r0c1_2});
        relaysMap.put(r_r0c1.getRelay_id(),r_r0c1);
        r_r0c2.setNodes(new Circle[] {node_r0c2_1,node_r0c2_2});
        relaysMap.put(r_r0c2.getRelay_id(),r_r0c2);
        r_r0c3.setNodes(new Circle[] {node_r0c3_1,node_r0c3_2});
        relaysMap.put(r_r0c3.getRelay_id(),r_r0c3);
        r_r0c4.setNodes(new Circle[] {node_r0c4_1,node_r0c4_2});
        relaysMap.put(r_r0c4.getRelay_id(),r_r0c4);
        r_r0c5.setNodes(new Circle[] {node_r0c5_1,node_r0c5_2});
        relaysMap.put(r_r0c5.getRelay_id(),r_r0c5);
        r_r0c6.setNodes(new Circle[] {node_r0c6_1,node_r0c6_2});
        relaysMap.put(r_r0c6.getRelay_id(),r_r0c6);
        r_r0c7.setNodes(new Circle[] {node_r0c7_1,node_r0c7_2});
        relaysMap.put(r_r0c7.getRelay_id(),r_r0c7);
        r_r0c8.setNodes(new Circle[] {node_r0c8_1,node_r0c8_2});
        relaysMap.put(r_r0c8.getRelay_id(),r_r0c8);
        r_r0c9.setNodes(new Circle[] {node_r0c9_1,node_r0c9_2});
        relaysMap.put(r_r0c9.getRelay_id(),r_r0c9);
        r_r0c10.setNodes(new Circle[] {node_r0c10_1,node_r0c10_2});
        relaysMap.put(r_r0c10.getRelay_id(),r_r0c10);
        r_r0c11.setNodes(new Circle[] {node_r0c11_1,node_r0c11_2});
        relaysMap.put(r_r0c11.getRelay_id(),r_r0c11);
        r_r0c12.setNodes(new Circle[] {node_r0c12_1,node_r0c12_2});
        relaysMap.put(r_r0c12.getRelay_id(),r_r0c12);
        r_r0c13.setNodes(new Circle[] {node_r0c13_1,node_r0c13_2});
        relaysMap.put(r_r0c13.getRelay_id(),r_r0c13);
        r_r0c14.setNodes(new Circle[] {node_r0c14_1,node_r0c14_2});
        relaysMap.put(r_r0c14.getRelay_id(),r_r0c14);
        r_r0c15.setNodes(new Circle[] {node_r0c15_1,node_r0c15_2});
        relaysMap.put(r_r0c15.getRelay_id(),r_r0c15);
        r_r1c0.setNodes(new Circle[] {node_r1c0_1,node_r1c0_2});
        relaysMap.put(r_r1c0.getRelay_id(),r_r1c0);
        r_r1c1.setNodes(new Circle[] {node_r1c1_1,node_r1c1_2});
        relaysMap.put(r_r1c1.getRelay_id(),r_r1c1);
        r_r1c2.setNodes(new Circle[] {node_r1c2_1,node_r1c2_2});
        relaysMap.put(r_r1c2.getRelay_id(),r_r1c2);
        r_r1c3.setNodes(new Circle[] {node_r1c3_1,node_r1c3_2});
        relaysMap.put(r_r1c3.getRelay_id(),r_r1c3);
        r_r1c4.setNodes(new Circle[] {node_r1c4_1,node_r1c4_2});
        relaysMap.put(r_r1c4.getRelay_id(),r_r1c4);
        r_r1c5.setNodes(new Circle[] {node_r1c5_1,node_r1c5_2});
        relaysMap.put(r_r1c5.getRelay_id(),r_r1c5);
        r_r1c6.setNodes(new Circle[] {node_r1c6_1,node_r1c6_2});
        relaysMap.put(r_r1c6.getRelay_id(),r_r1c6);
        r_r1c7.setNodes(new Circle[] {node_r1c7_1,node_r1c7_2});
        relaysMap.put(r_r1c7.getRelay_id(),r_r1c7);
        r_r1c8.setNodes(new Circle[] {node_r1c8_1,node_r1c8_2});
        relaysMap.put(r_r1c8.getRelay_id(),r_r1c8);
        r_r1c9.setNodes(new Circle[] {node_r1c9_1,node_r1c9_2});
        relaysMap.put(r_r1c9.getRelay_id(),r_r1c9);
        r_r1c10.setNodes(new Circle[] {node_r1c10_1,node_r1c10_2});
        relaysMap.put(r_r1c10.getRelay_id(),r_r1c10);
        r_r1c11.setNodes(new Circle[] {node_r1c11_1,node_r1c11_2});
        relaysMap.put(r_r1c11.getRelay_id(),r_r1c11);
        r_r1c12.setNodes(new Circle[] {node_r1c12_1,node_r1c12_2});
        relaysMap.put(r_r1c12.getRelay_id(),r_r1c12);
        r_r1c13.setNodes(new Circle[] {node_r1c13_1,node_r1c13_2});
        relaysMap.put(r_r1c13.getRelay_id(),r_r1c13);
        r_r1c14.setNodes(new Circle[] {node_r1c14_1,node_r1c14_2});
        relaysMap.put(r_r1c14.getRelay_id(),r_r1c14);
        r_r1c15.setNodes(new Circle[] {node_r1c15_1,node_r1c15_2});
        relaysMap.put(r_r1c15.getRelay_id(),r_r1c15);
        r_r2c0.setNodes(new Circle[] {node_r2c0_1,node_r2c0_2});
        relaysMap.put(r_r2c0.getRelay_id(),r_r2c0);
        r_r2c1.setNodes(new Circle[] {node_r2c1_1,node_r2c1_2});
        relaysMap.put(r_r2c1.getRelay_id(),r_r2c1);
        r_r2c2.setNodes(new Circle[] {node_r2c2_1,node_r2c2_2});
        relaysMap.put(r_r2c2.getRelay_id(),r_r2c2);
        r_r2c3.setNodes(new Circle[] {node_r2c3_1,node_r2c3_2});
        relaysMap.put(r_r2c3.getRelay_id(),r_r2c3);
        r_r2c4.setNodes(new Circle[] {node_r2c4_1,node_r2c4_2});
        relaysMap.put(r_r2c4.getRelay_id(),r_r2c4);
        r_r2c5.setNodes(new Circle[] {node_r2c5_1,node_r2c5_2});
        relaysMap.put(r_r2c5.getRelay_id(),r_r2c5);
        r_r2c6.setNodes(new Circle[] {node_r2c6_1,node_r2c6_2});
        relaysMap.put(r_r2c6.getRelay_id(),r_r2c6);
        r_r2c7.setNodes(new Circle[] {node_r2c7_1,node_r2c7_2});
        relaysMap.put(r_r2c7.getRelay_id(),r_r2c7);
        r_r2c8.setNodes(new Circle[] {node_r2c8_1,node_r2c8_2});
        relaysMap.put(r_r2c8.getRelay_id(),r_r2c8);
        r_r2c9.setNodes(new Circle[] {node_r2c9_1,node_r2c9_2});
        relaysMap.put(r_r2c9.getRelay_id(),r_r2c9);
        r_r2c10.setNodes(new Circle[] {node_r2c10_1,node_r2c10_2});
        relaysMap.put(r_r2c10.getRelay_id(),r_r2c10);
        r_r2c11.setNodes(new Circle[] {node_r2c11_1,node_r2c11_2});
        relaysMap.put(r_r2c11.getRelay_id(),r_r2c11);
        r_r2c12.setNodes(new Circle[] {node_r2c12_1,node_r2c12_2});
        relaysMap.put(r_r2c12.getRelay_id(),r_r2c12);
        r_r2c13.setNodes(new Circle[] {node_r2c13_1,node_r2c13_2});
        relaysMap.put(r_r2c13.getRelay_id(),r_r2c13);
        r_r2c14.setNodes(new Circle[] {node_r2c14_1,node_r2c14_2});
        relaysMap.put(r_r2c14.getRelay_id(),r_r2c14);
        r_r2c15.setNodes(new Circle[] {node_r2c15_1,node_r2c15_2});
        relaysMap.put(r_r2c15.getRelay_id(),r_r2c15);
        r_r3c0.setNodes(new Circle[] {node_r3c0_1,node_r3c0_2});
        relaysMap.put(r_r3c0.getRelay_id(),r_r3c0);
        r_r3c1.setNodes(new Circle[] {node_r3c1_1,node_r3c1_2});
        relaysMap.put(r_r3c1.getRelay_id(),r_r3c1);
        r_r3c2.setNodes(new Circle[] {node_r3c2_1,node_r3c2_2});
        relaysMap.put(r_r3c2.getRelay_id(),r_r3c2);
        r_r3c3.setNodes(new Circle[] {node_r3c3_1,node_r3c3_2});
        relaysMap.put(r_r3c3.getRelay_id(),r_r3c3);
        r_r3c4.setNodes(new Circle[] {node_r3c4_1,node_r3c4_2});
        relaysMap.put(r_r3c4.getRelay_id(),r_r3c4);
        r_r3c5.setNodes(new Circle[] {node_r3c5_1,node_r3c5_2});
        relaysMap.put(r_r3c5.getRelay_id(),r_r3c5);
        r_r3c6.setNodes(new Circle[] {node_r3c6_1,node_r3c6_2});
        relaysMap.put(r_r3c6.getRelay_id(),r_r3c6);
        r_r3c7.setNodes(new Circle[] {node_r3c7_1,node_r3c7_2});
        relaysMap.put(r_r3c7.getRelay_id(),r_r3c7);
        r_r3c8.setNodes(new Circle[] {node_r3c8_1,node_r3c8_2});
        relaysMap.put(r_r3c8.getRelay_id(),r_r3c8);
        r_r3c9.setNodes(new Circle[] {node_r3c9_1,node_r3c9_2});
        relaysMap.put(r_r3c9.getRelay_id(),r_r3c9);
        r_r3c10.setNodes(new Circle[] {node_r3c10_1,node_r3c10_2});
        relaysMap.put(r_r3c10.getRelay_id(),r_r3c10);
        r_r3c11.setNodes(new Circle[] {node_r3c11_1,node_r3c11_2});
        relaysMap.put(r_r3c11.getRelay_id(),r_r3c11);
        r_r3c12.setNodes(new Circle[] {node_r3c12_1,node_r3c12_2});
        relaysMap.put(r_r3c12.getRelay_id(),r_r3c12);
        r_r3c13.setNodes(new Circle[] {node_r3c13_1,node_r3c13_2});
        relaysMap.put(r_r3c13.getRelay_id(),r_r3c13);
        r_r3c14.setNodes(new Circle[] {node_r3c14_1,node_r3c14_2});
        relaysMap.put(r_r3c14.getRelay_id(),r_r3c14);
        r_r3c15.setNodes(new Circle[] {node_r3c15_1,node_r3c15_2});
        relaysMap.put(r_r3c15.getRelay_id(),r_r3c15);
        r_r4c0.setNodes(new Circle[] {node_r4c0_1,node_r4c0_2});
        relaysMap.put(r_r4c0.getRelay_id(),r_r4c0);
        r_r4c1.setNodes(new Circle[] {node_r4c1_1,node_r4c1_2});
        relaysMap.put(r_r4c1.getRelay_id(),r_r4c1);
        r_r4c2.setNodes(new Circle[] {node_r4c2_1,node_r4c2_2});
        relaysMap.put(r_r4c2.getRelay_id(),r_r4c2);
        r_r4c3.setNodes(new Circle[] {node_r4c3_1,node_r4c3_2});
        relaysMap.put(r_r4c3.getRelay_id(),r_r4c3);
        r_r4c4.setNodes(new Circle[] {node_r4c4_1,node_r4c4_2});
        relaysMap.put(r_r4c4.getRelay_id(),r_r4c4);
        r_r4c5.setNodes(new Circle[] {node_r4c5_1,node_r4c5_2});
        relaysMap.put(r_r4c5.getRelay_id(),r_r4c5);
        r_r4c6.setNodes(new Circle[] {node_r4c6_1,node_r4c6_2});
        relaysMap.put(r_r4c6.getRelay_id(),r_r4c6);
        r_r4c7.setNodes(new Circle[] {node_r4c7_1,node_r4c7_2});
        relaysMap.put(r_r4c7.getRelay_id(),r_r4c7);
        r_r4c8.setNodes(new Circle[] {node_r4c8_1,node_r4c8_2});
        relaysMap.put(r_r4c8.getRelay_id(),r_r4c8);
        r_r4c9.setNodes(new Circle[] {node_r4c9_1,node_r4c9_2});
        relaysMap.put(r_r4c9.getRelay_id(),r_r4c9);
        r_r4c10.setNodes(new Circle[] {node_r4c10_1,node_r4c10_2});
        relaysMap.put(r_r4c10.getRelay_id(),r_r4c10);
        r_r4c11.setNodes(new Circle[] {node_r4c11_1,node_r4c11_2});
        relaysMap.put(r_r4c11.getRelay_id(),r_r4c11);
        r_r4c12.setNodes(new Circle[] {node_r4c12_1,node_r4c12_2});
        relaysMap.put(r_r4c12.getRelay_id(),r_r4c12);
        r_r4c13.setNodes(new Circle[] {node_r4c13_1,node_r4c13_2});
        relaysMap.put(r_r4c13.getRelay_id(),r_r4c13);
        r_r4c14.setNodes(new Circle[] {node_r4c14_1,node_r4c14_2});
        relaysMap.put(r_r4c14.getRelay_id(),r_r4c14);
        r_r4c15.setNodes(new Circle[] {node_r4c15_1,node_r4c15_2});
        relaysMap.put(r_r4c15.getRelay_id(),r_r4c15);
        r_r5c0.setNodes(new Circle[] {node_r5c0_1,node_r5c0_2});
        relaysMap.put(r_r5c0.getRelay_id(),r_r5c0);
        r_r5c1.setNodes(new Circle[] {node_r5c1_1,node_r5c1_2});
        relaysMap.put(r_r5c1.getRelay_id(),r_r5c1);
        r_r5c2.setNodes(new Circle[] {node_r5c2_1,node_r5c2_2});
        relaysMap.put(r_r5c2.getRelay_id(),r_r5c2);
        r_r5c3.setNodes(new Circle[] {node_r5c3_1,node_r5c3_2});
        relaysMap.put(r_r5c3.getRelay_id(),r_r5c3);
        r_r5c4.setNodes(new Circle[] {node_r5c4_1,node_r5c4_2});
        relaysMap.put(r_r5c4.getRelay_id(),r_r5c4);
        r_r5c5.setNodes(new Circle[] {node_r5c5_1,node_r5c5_2});
        relaysMap.put(r_r5c5.getRelay_id(),r_r5c5);
        r_r5c6.setNodes(new Circle[] {node_r5c6_1,node_r5c6_2});
        relaysMap.put(r_r5c6.getRelay_id(),r_r5c6);
        r_r5c7.setNodes(new Circle[] {node_r5c7_1,node_r5c7_2});
        relaysMap.put(r_r5c7.getRelay_id(),r_r5c7);
        r_r5c8.setNodes(new Circle[] {node_r5c8_1,node_r5c8_2});
        relaysMap.put(r_r5c8.getRelay_id(),r_r5c8);
        r_r5c9.setNodes(new Circle[] {node_r5c9_1,node_r5c9_2});
        relaysMap.put(r_r5c9.getRelay_id(),r_r5c9);
        r_r5c10.setNodes(new Circle[] {node_r5c10_1,node_r5c10_2});
        relaysMap.put(r_r5c10.getRelay_id(),r_r5c10);
        r_r5c11.setNodes(new Circle[] {node_r5c11_1,node_r5c11_2});
        relaysMap.put(r_r5c11.getRelay_id(),r_r5c11);
        r_r5c12.setNodes(new Circle[] {node_r5c12_1,node_r5c12_2});
        relaysMap.put(r_r5c12.getRelay_id(),r_r5c12);
        r_r5c13.setNodes(new Circle[] {node_r5c13_1,node_r5c13_2});
        relaysMap.put(r_r5c13.getRelay_id(),r_r5c13);
        r_r5c14.setNodes(new Circle[] {node_r5c14_1,node_r5c14_2});
        relaysMap.put(r_r5c14.getRelay_id(),r_r5c14);
        r_r5c15.setNodes(new Circle[] {node_r5c15_1,node_r5c15_2});
        relaysMap.put(r_r5c15.getRelay_id(),r_r5c15);
        r_r6c0.setNodes(new Circle[] {node_r6c0_1,node_r6c0_2});
        relaysMap.put(r_r6c0.getRelay_id(),r_r6c0);
        r_r6c1.setNodes(new Circle[] {node_r6c1_1,node_r6c1_2});
        relaysMap.put(r_r6c1.getRelay_id(),r_r6c1);
        r_r6c2.setNodes(new Circle[] {node_r6c2_1,node_r6c2_2});
        relaysMap.put(r_r6c2.getRelay_id(),r_r6c2);
        r_r6c3.setNodes(new Circle[] {node_r6c3_1,node_r6c3_2});
        relaysMap.put(r_r6c3.getRelay_id(),r_r6c3);
        r_r6c4.setNodes(new Circle[] {node_r6c4_1,node_r6c4_2});
        relaysMap.put(r_r6c4.getRelay_id(),r_r6c4);
        r_r6c5.setNodes(new Circle[] {node_r6c5_1,node_r6c5_2});
        relaysMap.put(r_r6c5.getRelay_id(),r_r6c5);
        r_r6c6.setNodes(new Circle[] {node_r6c6_1,node_r6c6_2});
        relaysMap.put(r_r6c6.getRelay_id(),r_r6c6);
        r_r6c7.setNodes(new Circle[] {node_r6c7_1,node_r6c7_2});
        relaysMap.put(r_r6c7.getRelay_id(),r_r6c7);
        r_r6c8.setNodes(new Circle[] {node_r6c8_1,node_r6c8_2});
        relaysMap.put(r_r6c8.getRelay_id(),r_r6c8);
        r_r6c9.setNodes(new Circle[] {node_r6c9_1,node_r6c9_2});
        relaysMap.put(r_r6c9.getRelay_id(),r_r6c9);
        r_r6c10.setNodes(new Circle[] {node_r6c10_1,node_r6c10_2});
        relaysMap.put(r_r6c10.getRelay_id(),r_r6c10);
        r_r6c11.setNodes(new Circle[] {node_r6c11_1,node_r6c11_2});
        relaysMap.put(r_r6c11.getRelay_id(),r_r6c11);
        r_r6c12.setNodes(new Circle[] {node_r6c12_1,node_r6c12_2});
        relaysMap.put(r_r6c12.getRelay_id(),r_r6c12);
        r_r6c13.setNodes(new Circle[] {node_r6c13_1,node_r6c13_2});
        relaysMap.put(r_r6c13.getRelay_id(),r_r6c13);
        r_r6c14.setNodes(new Circle[] {node_r6c14_1,node_r6c14_2});
        relaysMap.put(r_r6c14.getRelay_id(),r_r6c14);
        r_r6c15.setNodes(new Circle[] {node_r6c15_1,node_r6c15_2});
        relaysMap.put(r_r6c15.getRelay_id(),r_r6c15);
        r_r7c0.setNodes(new Circle[] {node_r7c0_1,node_r7c0_2});
        relaysMap.put(r_r7c0.getRelay_id(),r_r7c0);
        r_r7c1.setNodes(new Circle[] {node_r7c1_1,node_r7c1_2});
        relaysMap.put(r_r7c1.getRelay_id(),r_r7c1);
        r_r7c2.setNodes(new Circle[] {node_r7c2_1,node_r7c2_2});
        relaysMap.put(r_r7c2.getRelay_id(),r_r7c2);
        r_r7c3.setNodes(new Circle[] {node_r7c3_1,node_r7c3_2});
        relaysMap.put(r_r7c3.getRelay_id(),r_r7c3);
        r_r7c4.setNodes(new Circle[] {node_r7c4_1,node_r7c4_2});
        relaysMap.put(r_r7c4.getRelay_id(),r_r7c4);
        r_r7c5.setNodes(new Circle[] {node_r7c5_1,node_r7c5_2});
        relaysMap.put(r_r7c5.getRelay_id(),r_r7c5);
        r_r7c6.setNodes(new Circle[] {node_r7c6_1,node_r7c6_2});
        relaysMap.put(r_r7c6.getRelay_id(),r_r7c6);
        r_r7c7.setNodes(new Circle[] {node_r7c7_1,node_r7c7_2});
        relaysMap.put(r_r7c7.getRelay_id(),r_r7c7);
        r_r7c8.setNodes(new Circle[] {node_r7c8_1,node_r7c8_2});
        relaysMap.put(r_r7c8.getRelay_id(),r_r7c8);
        r_r7c9.setNodes(new Circle[] {node_r7c9_1,node_r7c9_2});
        relaysMap.put(r_r7c9.getRelay_id(),r_r7c9);
        r_r7c10.setNodes(new Circle[] {node_r7c10_1,node_r7c10_2});
        relaysMap.put(r_r7c10.getRelay_id(),r_r7c10);
        r_r7c11.setNodes(new Circle[] {node_r7c11_1,node_r7c11_2});
        relaysMap.put(r_r7c11.getRelay_id(),r_r7c11);
        r_r7c12.setNodes(new Circle[] {node_r7c12_1,node_r7c12_2});
        relaysMap.put(r_r7c12.getRelay_id(),r_r7c12);
        r_r7c13.setNodes(new Circle[] {node_r7c13_1,node_r7c13_2});
        relaysMap.put(r_r7c13.getRelay_id(),r_r7c13);
        r_r7c14.setNodes(new Circle[] {node_r7c14_1,node_r7c14_2});
        relaysMap.put(r_r7c14.getRelay_id(),r_r7c14);
        r_r7c15.setNodes(new Circle[] {node_r7c15_1,node_r7c15_2});
        relaysMap.put(r_r7c15.getRelay_id(),r_r7c15);

        //Настрока объектов источников и добавление в карту
        Map<String, Object> PowerSourcesMap = new HashMap<String, Object>();
        objects.put("powerSources", PowerSourcesMap);
        //настройка И1
        I1.VSourceIcon = VSB_I1;
        I1.VMeasIcon = VMB_I1;
        I1.ISourceIcon = ISB_I1;
        I1.IMeasIcon = IMB_I1;
        PowerSourcesMap.put(I1.sourceId, I1);
        I2.VSourceIcon = VSB_I2;
        I2.VMeasIcon = VMB_I2;
        I2.ISourceIcon = ISB_I2;
        I2.IMeasIcon = IMB_I2;
        PowerSourcesMap.put(I2.sourceId, I2);
        I3.VSourceIcon = VSB_I3;
        I3.VMeasIcon = VMB_I3;
        I3.ISourceIcon = ISB_I3;
        I3.IMeasIcon = IMB_I3;
        PowerSourcesMap.put(I3.sourceId, I3);
        I4.VSourceIcon = VSB_I4;
        I4.VMeasIcon = VMB_I4;
        I4.ISourceIcon = ISB_I4;
        I4.IMeasIcon = IMB_I4;
        PowerSourcesMap.put(I4.sourceId, I4);
    }

    public void shutdown() {
        if (editing) {
            try {
                if (currProgram.ProgramSaved?false:twoStateDialog("Сохранить текущую программу?")) {
                    //алгоритм сохранения текущей прогрраммы
                    currProgram.SaveProgramFile();
                }
            } catch (Exception e) {
                Log.info("Shutdown Error: ");
                e.printStackTrace();
            }
            Platform.exit();
        }
    }

    public void setLogger(Logger logger) {
        this.Log = logger;
    }
}

