package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Logger logger = Logger.getLogger("AIKEditorLog.log");
        FileHandler fh;

        try {

            // This block configure the logger with handler and formatter
            fh = new FileHandler("C:/TestInstruments/AIK/AIKEditorLog.log");
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FXMLLoader loader = new FXMLLoader(getClass().getResource("views/main_window.fxml"));
        Parent root = loader.load();
        MainProgramEditWindowController controller = loader.getController();
        Scene scene = new Scene(root, 1700, 920);
        Stage stage = new Stage();
        stage.setScene(scene);
        controller.setLogger(logger);
        stage.getIcons().clear();
        stage.getIcons().add(new Image(Main.class.getResourceAsStream("images/main.png")));
        stage.setOnCloseRequest(e -> {
            controller.shutdown();
        });
        stage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
