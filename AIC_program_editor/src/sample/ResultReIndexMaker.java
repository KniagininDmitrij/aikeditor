package sample;

import javafx.scene.control.ListView;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.List;

public class ResultReIndexMaker {

    public static String GetIndexedExpString(String text, List<Node> measNodes){
        List<String> measureStrings = getMeasStringList(measNodes);
        text = text.trim();
        int measuresCount = measureStrings.size();
        for (int i = 0; i < measuresCount; i++){
            text = text.replace(measureStrings.get(i),"#"+i);
        }
        checkIndexedExpString(text);
        return text;
    }

    private static void checkIndexedExpString(String text) {
        final String regex = "[ \\+-/\\*#\\d([Ee][+-]\\d+)]+";
        if(!text.matches(regex)){
            throw new IllegalArgumentException("Result expression isn't indexed correctly");
        }
    }

    public static List<String> getMeasStringList(List<Node> measNodes) {
        List<String> resultList = new ArrayList<>();
        for (int i = measNodes.size() - 1; i >= 0; i--){
            Node node = measNodes.get(i);
            String measStr = sameItemsCountAdder(new String[]{(node.getNodeName().equals("currMeas")?"I_":"V_")+
                    node.getAttributes().getNamedItem("id").getNodeValue(),"("+node.getParentNode().getAttributes().getNamedItem("name").getNodeValue()+")"}, resultList);
            resultList.add(measStr);
        }
        return resultList;
    }

    private static String sameItemsCountAdder(String[] strArr2, List<String> valuesList){
        int counter = 1;
        for (String item : valuesList){
            if (item.contains(strArr2[0])&&item.contains(strArr2[1])){
                counter++;
            }
        }
        return strArr2[0]+(counter>1?"_"+Integer.toString(counter):"")+strArr2[1];
    }

}
