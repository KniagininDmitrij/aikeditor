package sample;

import java.util.HashMap;
import java.util.Map;

public  class DAQ {
    private final static Map<String, String> Ports = new HashMap<String, String>()
    {
        {
            put("AI 0", "AI_0");
            put("AI 1", "AI_1");
            put("AO 0", "AO_0");
            put("AO 1", "AO_1");
            put("PFI 0", "PFI_0");
            put("PFI 6", "PFI_6");
            put("PFI 9", "PFI_9");
        };
    };
    private final static Map<String, String[]> FunctionsIdsMap = new HashMap<String, String[]>()
    {
        {
            put("AI_0", new String[]{"f1", "f2"});
            put("AI_1", new String[]{"f1", "f2"});
            put("AO_0", new String[]{"f3", "f4"});
            put("AO_1", new String[]{"f3", "f4"});
            put("PFI_0", new String[]{"f5", "f6"});
            put("PFI_6", new String[]{"f7", "f8"});
            put("PFI_9", new String[]{"f7", "f8"});
        };
    };
    private final static Map<String, String> FunctionsNames = new HashMap<String, String>()
    {
        {
            put("f1", "Засечь уровень лог.ед.");
            put("f2", "Засечь уровень лог.нуля");
            put("f3", "Выдать уровень лог.ед.");
            put("f4", "Выдать уровень лог.нуля");
            put("f5", "Засечь уровень лог.ед.");
            put("f6", "Засечь уровень лог.нуля");
            put("f7", "Выдать уровень лог.ед.");
            put("f8", "Выдать уровень лог.нуля");
        };
    };

    public static String[] getPortsList() {
        int lent = Ports.keySet().toArray().length;
        String[] result = new String[lent];
        int counter = 0;
        for (Object s:Ports.keySet().toArray()){
            result[counter]=(String)s;
            counter++;
        }

        return result;
    }

    public static Map<String, String[]> getFunctionsIdsMap() {
        return FunctionsIdsMap;
    }

    public static Map<String, String> getFunctionsNames() {
        return FunctionsNames;
    }

    public static String[] getFuncNameListByPortName(String portName){
       String id = Ports.get(portName);
       String[] funcList = FunctionsIdsMap.get(id);
       for (int i=0;i<funcList.length;i++){
           funcList[i] = FunctionsNames.get(funcList[i]);
       }
       return funcList;
    }
}
