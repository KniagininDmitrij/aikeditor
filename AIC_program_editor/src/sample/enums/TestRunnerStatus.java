package sample.enums;

public enum TestRunnerStatus {
    INIT, RUN, END, FAIL, ERROR
}
