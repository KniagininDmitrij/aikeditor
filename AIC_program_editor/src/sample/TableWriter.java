package sample;

import sample.controllers.execPrgController;
import sample.enums.TestRunnerStatus;

import java.util.List;
import java.util.logging.Logger;

public class TableWriter implements Runnable{
    private int finalRunCount;
    private List<String> finalParamNamesList;
    private boolean finalHasNames;
    private String fileName;
    private Logger Log;
    private execPrgController controller;
    private boolean needStop;
    private TestRunner testRunner;

    public TableWriter(int finalRunCount, List<String> finalParamNamesList, boolean finalHasNames, String fileName, Logger log, execPrgController controller, TestRunner testRunner) {
        this.finalRunCount = finalRunCount;
        this.finalParamNamesList = finalParamNamesList;
        this.finalHasNames = finalHasNames;
        this.fileName = fileName;
        Log = log;
        this.controller = controller;
        this.testRunner = testRunner;
        needStop = false;
    }

    public void stop(){
        needStop = true;
    }

    @Override
    public void run() {
        int nameIndex = 0;
        try {
            while (ResultParser.getCurrentReturnedCount() < finalRunCount
                    && !needStop
                    && testRunner.getTestRunnerStatus() != TestRunnerStatus.ERROR
                    && testRunner.getTestRunnerStatus() != TestRunnerStatus.FAIL) {
                for (Result result : ResultParser.getNewResultIfExist()) {
                    String paramName = "";
                    try {
                        paramName = (finalHasNames ? finalParamNamesList.get(nameIndex++) : fileName.substring(0, fileName.lastIndexOf('.')));
                        if (nameIndex >= finalParamNamesList.size()) {
                            nameIndex = 0;
                        }
                    } catch (Exception exception) {
                        Log.info(exception.getLocalizedMessage());
                    } finally {
                        controller.addRow2ResultTable(paramName, result);
                    }
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception exception) {
            Log.info(exception.getMessage());
        }
        if (testRunner.getTestRunnerStatus() == TestRunnerStatus.FAIL){
            controller.setNeedStopExecution(true);
        }
        if (testRunner.getTestRunnerStatus() == TestRunnerStatus.ERROR){
            controller.setLastSuccess();
        }
        controller.runNextProgram();
    }
}
