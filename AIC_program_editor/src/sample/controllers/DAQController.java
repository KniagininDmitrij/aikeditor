package sample.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import sample.DAQ;
import sample.Methods;

public class DAQController {
    private String dialog_status = "open";

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button dialog_yes_butt;

    @FXML
    private Label dialog_Label;

    @FXML
    private Button dialog_no_butt;

    @FXML
    private ChoiceBox<String> portCB;

    @FXML
    private ChoiceBox<String> funcCB;

    @FXML
    private ScrollPane propertyScrollPane;

    @FXML
    private AnchorPane propertyPane;

    @FXML
    void initialize() {
        for (String port: DAQ.getPortsList()){
            portCB.getItems().add(port);
        }
        portCB.setOnAction(event -> {
            if (portCB.getValue()!=null) {
                funcCB.getItems().clear();
                for (String funcName : DAQ.getFuncNameListByPortName(portCB.getValue())) {
                    funcCB.getItems().add(funcName);
                }
            }
        });
        dialog_yes_butt.setOnAction(event -> {
            yesEnd();
        });
        dialog_no_butt.setOnAction(event -> {
            dialog_no_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка НЕТ");
            Stage stage = (Stage) dialog_no_butt.getScene().getWindow();
            dialog_status = "no";
            stage.close();
        });
    }

    private void yesEnd() {
        if (checkForm()){
            dialog_yes_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка ДА");
            Stage stage = (Stage) dialog_yes_butt.getScene().getWindow();
            dialog_status = "yes";
            stage.close();
        }else {
            try {
                Methods.messageDialog("Не все поля заполнены!");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean checkForm() {
        return true;
    }

    public String getDialog_status() {
        return dialog_status;
    }
}
