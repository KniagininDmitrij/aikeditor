package sample.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.*;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import sample.Methods;
import sample.external.workWithXMLStructure;

import javax.xml.parsers.ParserConfigurationException;

public class LibController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button dialog_yes_butt;

    @FXML
    private Label dialog_Label;

    @FXML
    private Button dialog_no_butt;

    @FXML
    private ListView<String> libsList;

    @FXML
    private ScrollPane scrollPane;

    @FXML
    private AnchorPane settPane;

    private String dialog_status = "open";
    private String path;
    private ArrayList<String> filesList;
    private ArrayList<Node> libVariantBlocks = new ArrayList<>();
    private ArrayList<ChoiceBox> choiceBoxes = new ArrayList<>();
    private Node resultNode;
    private Document LibDocument;
    private String libName;
    private HashMap<Node, Boolean> DocumentIsVariantBlockMap = new HashMap<>();
    private HashMap<Node, ChoiceBox> VariantBlockChoice = new HashMap<>();
    private List<Node> RetNodesList = new ArrayList<>();
    private Element Commute;

    public Element getResultEl() {
        return resultEl;
    }

    private Element resultEl;

    public void setStartData(String path, ArrayList<String> fileNames, int measCount){
        libsList.getItems().clear();
        int len = fileNames.toArray().length;
        for (int i=0;i<len;i++){
            libsList.getItems().add(fileNames.get(i));
        }
        this.path = path;
        this.filesList = fileNames;
    }

    @FXML
    void initialize() {
        libsList.setOnMouseClicked(click -> {
            if (click.getClickCount() == 1) {
                String fileName = libsList.getSelectionModel().getSelectedItem();
                printBlocks4selectedLib(fileName);
                libName = fileName;
            }
        });

        dialog_yes_butt.setOnAction(event -> {
            yesEnd();
        });
        dialog_no_butt.setOnAction(event -> {
            dialog_no_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка НЕТ");
            Stage stage = (Stage) dialog_no_butt.getScene().getWindow();
            dialog_status = "no";
            stage.close();
        });
    }

    private void printBlocks4selectedLib(String fileName) {
        //1.отрыть файл как дерево xml
        try {
            String filePath = this.path +'\\'+ fileName;
            LibDocument = new workWithXMLStructure().createNewXMLDocument(filePath);
        //2. прочитать и найти блоки в которых есть только параметры 1 2 3 .. 6
            int count = 0;
            settPane.getChildren().clear();
            VariantBlockChoice.clear();
            NodeList blocks = LibDocument.getElementsByTagName("block");
            for (int i = 0; i<blocks.getLength();i++){
                    Node block = blocks.item(i);
                    if (isLibVariantBlock(block)){
                        DocumentIsVariantBlockMap.put(block, true);
                        //3. вывести на панель таблицу в каждой строке блок + выпадающий список от 1 до 6
                        Label blockLabel = new Label();
                        ChoiceBox<String> variants = Methods.getVariantsCB(new String[]{"1","2","3","4","5","6"});

                        VariantBlockChoice.put(block, variants);

                        blockLabel.setText(block.getAttributes().getNamedItem("name").getNodeValue());
                        blockLabel.setLayoutX(2.0); variants.setLayoutX(150.0);
                        blockLabel.setLayoutY(2.0+25*count); variants.setLayoutY(2.0+25*count);

                        settPane.getChildren().add(blockLabel);
                        settPane.getChildren().add(variants);

                        count++;
                    }else{
                        DocumentIsVariantBlockMap.put(block, false);
                    }
            }

        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        }


        //4. запомнить выбранные позиции
        //5. сформировать элемент параметра с действиями библиотеки с учетом выбранных вариантов и передать наверх - это результат

    }

    private boolean isLibVariantBlock(Node node) {
        boolean result = false;
        NodeList nList = node.getChildNodes();
        int counter = 0;
        for (int i = 0; i<nList.getLength();i++){
            if (nList.item(i).getNodeType()== Node.ELEMENT_NODE) {
                Node N = nList.item(i);
                if (N.getNodeName().equals("param")){
                    try {
                        int variant = Integer.parseInt(N.getAttributes().getNamedItem("name").getNodeValue());
                        if (variant>0 && variant<=6){
                            result = true;
                            counter++;
                            continue;
                        }
                        result = false;
                        break;
                    }catch (Exception e){
                        result = false;
                        break;
                    }
                }
            }
        }
        if (counter!=6)
            result = false;
        return result;
    }

    private void yesEnd() {
        if (checkForm()){
            this.resultNode = createResultNode();
            dialog_yes_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка ДА");
            Stage stage = (Stage) dialog_yes_butt.getScene().getWindow();
            dialog_status = "yes";
            stage.close();
        }else {
            try {
                Methods.messageDialog("Не все поля заполнены!");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private Node createResultNode() {
        resultEl = LibDocument.createElement("block");
        resultEl.setAttribute("name", libName);

        Commute = LibDocument.createElement("param");
        Commute.setAttribute("name", "Коммутация");

        fillRetList();
        for (Node node: RetNodesList){
            resultEl.appendChild(node.cloneNode(true));
        }
        return resultEl;
    }

    private boolean checkForm() {
        Boolean result = true;
        //проверка выражения
        Iterator<ChoiceBox> iterator = choiceBoxes.iterator();
        while (iterator.hasNext()){
            ChoiceBox<String> choiceBox = iterator.next();
            if (choiceBox.getValue()==null){
                result = false;
                break;
            }
        }
        return result;
    }
    private static final Set<String> moreActionTegs = new HashSet<String>(Arrays.asList(
            new String[] {"block","param","header","stream"}
    ));

    private void fillRetList(){
        NodeList libElements = LibDocument.getChildNodes().item(0).getChildNodes();
        for (int elemInd = 0; elemInd < libElements.getLength(); elemInd++){
            Node libElem = libElements.item(elemInd);
            if (libElem.getNodeType() == Node.ELEMENT_NODE){
                if (libElem.getNodeName().equals("block")){
                    boolean isVariantBlock = DocumentIsVariantBlockMap.get(libElem);
                    if (isVariantBlock){
                        NodeList params = libElem.getChildNodes();
                        for (int paramIndex = 0; paramIndex < params.getLength(); paramIndex++){
                            Node param = params.item(paramIndex);
                            if (param.getNodeName().equals("param")) {
                                String paramVariant = param.getAttributes().getNamedItem("name").getNodeValue();
                                if (paramVariant.equals(VariantBlockChoice.get(libElem).getValue())){
                                    if(!Commute.hasChildNodes()){
                                        RetNodesList.add(Commute);
                                    }
                                    addElemChildren2Node(param, Commute);
                                }
                            }
                        }
                    }else{
                        addElemChildren2List(libElem, RetNodesList);
                    }
                }else{
                    if (!libElem.getNodeName().equals("stream")) {
                        RetNodesList.add(libElem);
                    }
                }
            }
        }
    }

    private void addElemChildren2Node(Node libElem, Element elem) {
        NodeList nodes = libElem.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++){
            Node node = nodes.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE){
                elem.appendChild(node);
            }
        }
    }

    private void addElemChildren2List(Node libElem, List<Node> retNodesList) {
        NodeList nodes = libElem.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++){
            Node node = nodes.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE){
                retNodesList.add(node);
            }
        }
    }

    public String getDialog_status() {
        return dialog_status;
    }
}
