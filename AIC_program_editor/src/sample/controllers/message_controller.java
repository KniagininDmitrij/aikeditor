package sample.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class message_controller {
    private String dialog_status = "open";
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button dialog_yes_butt;

    @FXML
    private Label dialog_text;

    public void setMessage(String mess){
        dialog_text.setText(mess);
    }
    public String getDialogStatus(){
        return dialog_status;
    }

    @FXML
    void initialize() {
        dialog_yes_butt.setText("Да");
        dialog_text.setWrapText(true);

        dialog_yes_butt.setOnAction(event -> {
            dialog_yes_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка ДА");
            Stage stage = (Stage) dialog_yes_butt.getScene().getWindow();
            dialog_status = "yes";
            stage.close();
        });

    }
}
