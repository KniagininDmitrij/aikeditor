package sample.controllers;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import sample.PowerSource;

public class voltMeasController {
    private String dialog_status = "open";

    private Map<String, String> values = new HashMap<String, String>();

    public Map<String, String> getValues() {
        return values;
    }

    public String getDialog_status() {
        return dialog_status;
    }

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button dialog_yes_butt;

    @FXML
    private Label dialog_Label;

    @FXML
    private Button dialog_no_butt;

    @FXML
    private Label textF_label1;

    @FXML
    private ChoiceBox<String> voltRanges;

    @FXML
    void initialize() {

        dialog_yes_butt.setOnAction(event -> {
            yesEnd();
        });
        dialog_no_butt.setOnAction(event -> {
            dialog_no_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка НЕТ");
            Stage stage = (Stage) dialog_no_butt.getScene().getWindow();
            dialog_status = "no";
            stage.close();
        });
    }

    public void setSource(PowerSource source){
        dialog_Label.setText(dialog_Label.getText()+" ("+source.sourceId+")");
        choiceBoxesSetting(source.getVoltageRanges());
    }
    public void setRange(String range) {
        voltRanges.setValue(range);
    }
    private void yesEnd() {
        if (checkForm()){
            values.put("range", voltRanges.getValue());

            dialog_yes_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка ДА");
            Stage stage = (Stage) dialog_yes_butt.getScene().getWindow();
            dialog_status = "yes";
            stage.close();
        }
    }

    private boolean checkForm() {
        return true;
    }

    private void choiceBoxesSetting(String[] voltageRanges) {
        voltRanges.getItems().clear();
        for (String range:voltageRanges){
            voltRanges.getItems().add(range);
        }
        voltRanges.setValue(voltRanges.getItems().get(0));
    }
}
