package sample.controllers;

        import java.net.URL;
        import java.util.ResourceBundle;
        import javafx.fxml.FXML;
        import javafx.scene.control.Button;
        import javafx.scene.control.Label;
        import javafx.scene.control.TextField;
        import javafx.scene.input.KeyCode;
        import javafx.stage.Stage;
        import sample.enums.DialogEndStatus;

public class YN_with_textIn_controller {

    public DialogEndStatus getDialog_status() {
        return dialog_status;
    }

    private DialogEndStatus dialog_status = DialogEndStatus.CLOSE;

    public String getInputText() {
        return inputText;
    }

    private String inputText = "";
    private String inputType;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button dialog_yes_butt;

    @FXML
    private Label dialog_Label;

    @FXML
    private Button dialog_no_butt;

    @FXML
    private TextField textField;

    @FXML
    private Label textF_label;

    @FXML
    void initialize() {
        dialog_yes_butt.setText("Да");
        dialog_no_butt.setText("Нет");
        dialog_Label.setWrapText(true);
        textF_label.setWrapText(true);


        dialog_yes_butt.setOnAction(event -> {
            yesEnd();
        });
        dialog_no_butt.setOnAction(event -> {
            dialog_no_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка НЕТ");
            Stage stage = (Stage) dialog_no_butt.getScene().getWindow();
            dialog_status = DialogEndStatus.NO;
            stage.close();
        });
        textField.setOnKeyPressed(ke->{
            KeyCode keyCode = ke.getCode();
            if (keyCode.equals(KeyCode.ENTER)){
                yesEnd();
            }
        });
    }
    private void yesEnd(){
        if (inputCheck()) {
            inputText=textField.getText();
            dialog_yes_butt.setStyle("-fx-background-color: #fafafa");
            Stage stage = (Stage) dialog_yes_butt.getScene().getWindow();
            dialog_status = DialogEndStatus.YES;
            stage.close();
        }
    }
    public void setContent(String Label, String textFieldName, String inputType){
        this.inputType=inputType;
        dialog_Label.setText(Label);
        textF_label.setText(textFieldName);
        double labelW = textFieldName.length()*(textF_label.getFont().getSize())*0.8;
        textF_label.setMinWidth(labelW);
        textField.setMaxWidth(320.0 - labelW);
        textField.setLayoutX(labelW-1);
        textF_label.setLayoutX(0);
        textField.requestFocus();
    }
    public void setContent(String Label, String textFieldName, String inputDef, String inputType){
        this.inputType=inputType;
        dialog_Label.setText(Label);
        textF_label.setText(textFieldName);
        double labelW = textFieldName.length()*(textF_label.getFont().getSize())*0.8;
        textF_label.setMinWidth(labelW);
        textField.setText(inputDef);
        textField.setMaxWidth(320.0 - labelW);
        textField.setLayoutX(labelW-1);
        textF_label.setLayoutX(0);
    }
    boolean inputCheck(){
        switch (inputType){
            case "int":
                try {
                    Integer.parseInt(textField.getText());
                    return true;
                }catch (Exception e){
                    textField.setText("Ошибка!");
                    return false;
                }
            case "string": default:
                return textField.getText().length()>0;
        }
    }
}
