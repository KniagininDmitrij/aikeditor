package sample.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import sample.enums.DialogEndStatus;

public class dialog_controller {

    private DialogEndStatus dialog_status = DialogEndStatus.OPEN;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button dialog_yes_butt;

    @FXML
    private Label dialog_text;

    @FXML
    private Button dialog_no_butt;

    public void setMessage(String mess){
        dialog_text.setText(mess);
    }
    public DialogEndStatus getDialogStatus(){
        return dialog_status;
    }

    @FXML
    void initialize() {
        dialog_yes_butt.setText("Да");
        dialog_no_butt.setText("Нет");
        dialog_yes_butt.requestFocus();
        dialog_text.setWrapText(true);
        dialog_yes_butt.setStyle("-fx-border-color: blue");
        dialog_status = DialogEndStatus.CLOSE;
        dialog_yes_butt.setOnKeyPressed(ke->{
            KeyCode keyCode = ke.getCode();
            if (keyCode.equals(KeyCode.ENTER)){
                yesEnd();
            }
        });
        dialog_no_butt.setOnKeyPressed(ke->{
            KeyCode keyCode = ke.getCode();
            if (keyCode.equals(KeyCode.ENTER)){
                noEnd();
            }
        });

        dialog_yes_butt.setOnAction(event -> {
            yesEnd();
        });
        dialog_no_butt.setOnAction(event -> {
            noEnd();
        });
    }
    private void yesEnd(){
        dialog_yes_butt.setStyle("-fx-background-color: #fafafa");
        System.out.println("В диалоге нажата кнопка ДА");
        Stage stage = (Stage) dialog_yes_butt.getScene().getWindow();
        dialog_status = DialogEndStatus.YES;
        stage.close();
    }
    private void noEnd(){
        dialog_no_butt.setStyle("-fx-background-color: #fafafa");
        System.out.println("В диалоге нажата кнопка НЕТ");
        Stage stage = (Stage) dialog_no_butt.getScene().getWindow();
        dialog_status = DialogEndStatus.NO;
        stage.close();
    }
}

