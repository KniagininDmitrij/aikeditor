package sample.controllers;

import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.scene.control.Button;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import sample.Methods;
import sample.PowerSource;
import sample.simple.SourceTimeMode;

public class currSourceController {

    private String dialog_status = "open";
    private Map<String, String> values = new HashMap<String, String>();
    private PowerSource source;

    public Map<String, String> getValues() {
        return values;
    }

    public String getDialog_status() {
        return dialog_status;
    }

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button dialog_yes_butt;

    @FXML
    private Label dialog_Label;

    @FXML
    private Button dialog_no_butt;

    @FXML
    private TextField currValueIn;

    @FXML
    private Label textF_label;

    @FXML
    private CheckBox resetCheckBox;

    @FXML
    private ChoiceBox<String> currUnitsV;

    @FXML
    private ChoiceBox<String> currRanges;

    @FXML
    private TextField voltLimitIn;

    @FXML
    private ChoiceBox<String> voltUnitsL;

    @FXML
    private ChoiceBox<String> timeModeList;

    @FXML
    private TextField comment;


    @FXML
    void initialize() {
        initForm();
        dialog_yes_butt.setOnAction(event -> {
            yesEnd();
        });
        dialog_no_butt.setOnAction(event -> {
            dialog_no_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка НЕТ");
            Stage stage = (Stage) dialog_no_butt.getScene().getWindow();
            dialog_status = "no";
            stage.close();
        });
    }
    public void setSource(PowerSource source){
        this.source = source;
        dialog_Label.setText(dialog_Label.getText()+" ("+source.sourceId+")");
        choiceBoxesSetting(source.getCurrentRanges());
    }
    public void setValues(Map<String, String> values) {
        this.values = values;
        if (values.get("status").equals("current")){
            resetCheckBox.setSelected(false);
            currValueIn.setText(values.get("current").split(" ")[0]);
            currUnitsV.setValue(values.get("current").split(" ")[1]);
            currRanges.setValue(values.get("range"));
            voltLimitIn.setText(values.get("voltageLim").split(" ")[0]);
            voltUnitsL.setValue(values.get("voltageLim").split(" ")[1]);
            timeModeList.setValue(values.getOrDefault("timeMode", SourceTimeMode.NORMAL.name()));
        }else{
            resetCheckBox.setSelected(true);
        }
        comment.setText(values.get("comment"));
    }
    private void yesEnd() {
        if (checkForm()){
            if (resetCheckBox.isSelected()==false) {
                values.put("status", "current");
                values.put("current", currValueIn.getText() + " " + currUnitsV.getValue());
                values.put("range", currRanges.getValue());
                values.put("voltageLim", voltLimitIn.getText() + " " + voltUnitsL.getValue());
                values.put("voltageLimRange", source.getRangeForValue(values.get("voltageLim")));
                values.put("timeMode", timeModeList.getValue());
            }else values.put("status", "reset");
            values.put("comment", comment.getText());

            dialog_yes_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка ДА");
            Stage stage = (Stage) dialog_yes_butt.getScene().getWindow();
            dialog_status = "yes";
            stage.close();
        }
    }

    private void choiceBoxesSetting(String[] currentRanges) {
        voltUnitsL.getItems().clear();
        voltUnitsL.getItems().add("mV");
        voltUnitsL.getItems().add("V");
        voltUnitsL.setValue(voltUnitsL.getItems().get(1));
        voltUnitsL.show();

        currRanges.getItems().clear();
        for (String range:currentRanges){
            currRanges.getItems().add(range);
        }
        currRanges.setValue("mA");
        //currRanges.setValue(currRanges.getItems().get(0));

        currUnitsV.getItems().clear();
        currUnitsV.getItems().add("uA");
        currUnitsV.getItems().add("mA");
        currUnitsV.getItems().add("A");
        currUnitsV.setValue("mA");
    }

    boolean checkForm(){
        boolean result = true;
        if (resetCheckBox.isSelected()==false) {
            result &= checkFloatField(currValueIn, currUnitsV.getValue(), source.maxCurrent);
            result &= checkFloatField(voltLimitIn, voltUnitsL.getValue(), source.maxVoltage);
        }
        return result;
    }
    private boolean checkFloatField(TextField field, String unit, Float max) {
        Boolean result = true;
        try {
            float value = Float.parseFloat(field.getText());
            if (unit.length() > 1)
                value *= Methods.getMultSi(unit.substring(0, unit.length() - 1));
            if (value > max) {
                result = false;
                field.setText("Много!");
            }//обязательно сделать в зависимости от
        } catch (Exception e) {
            field.setText("Ошибка!");
            result = false;
        }
        return result;
    }

    private void initForm(){
        timeModeList.getItems().clear();
        Arrays.stream(SourceTimeMode.values()).forEach(t -> timeModeList.getItems().add(t.name()));
        timeModeList.setValue(SourceTimeMode.NORMAL.name());
    }
}
