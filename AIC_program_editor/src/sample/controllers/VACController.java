package sample.controllers;

import java.net.URL;
import java.util.*;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.Methods;

public class VACController {
    private HashMap<String,String> OutValues = new HashMap<String,String>();
    private String dialog_status = "open";

    public String getDialog_status() {
        return dialog_status;
    }
    public HashMap<String, String> getOutValues() {
        return OutValues;
    }

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button dialog_yes_butt;

    @FXML
    private Label dialog_Label;

    @FXML
    private Button dialog_no_butt;

    @FXML
    private TextField maxSourceValueMod;

    @FXML
    private CheckBox need2PlotCheckBox;

    @FXML
    private CheckBox forTwoDirectionCheckBox;

    @FXML
    private TextField sourceStepValue;

    @FXML
    private ChoiceBox<String> sourceSelector;

    @FXML
    private TextField X_mult;

    @FXML
    private TextField X_unit;

    @FXML
    private TextField X_label;

    @FXML
    private ChoiceBox<String> measureSelector;

    @FXML
    private TextField Y_unit;

    @FXML
    private TextField Y_label;

    @FXML
    private TextField Y_mult;

    @FXML
    private TextField startDelay;

    @FXML
    void initialize() {
        dialog_yes_butt.setOnAction(event -> {
            yesEnd();
        });
        dialog_no_butt.setOnAction(event -> {
            dialog_no_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка НЕТ");
            Stage stage = (Stage) dialog_no_butt.getScene().getWindow();
            dialog_status = "no";
            stage.close();
        });

    }

    private void yesEnd() {
        if (checkForm()){
            OutValues.put("sourceID",sourceSelector.getValue());
            OutValues.put("sourceStartValue", forTwoDirectionCheckBox.isSelected()?Float.toString(-1.0f*Float.parseFloat(maxSourceValueMod.getText())):"0.0");
            OutValues.put("sourceEndValue", Float.toString(Float.parseFloat(maxSourceValueMod.getText())));
            OutValues.put("sourceStep",sourceStepValue.getText());
            OutValues.put("plotGainX",X_mult.getText());
            OutValues.put("labelX",X_label.getText());
            OutValues.put("unitX",X_unit.getText());
            OutValues.put("measureID",measureSelector.getValue());
            OutValues.put("plotGainY",Y_mult.getText());
            OutValues.put("labelY",Y_label.getText());
            OutValues.put("unitY",Y_unit.getText());
            OutValues.put("measureDelay",startDelay.getText());
            OutValues.put("needPlot",(need2PlotCheckBox.isSelected()?"yes":"no"));

            dialog_yes_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка ДА");
            Stage stage = (Stage) dialog_yes_butt.getScene().getWindow();
            dialog_status = "yes";
            stage.close();
        }
    }
    public void setValues(HashMap<String,String> values){
        sourceSelector.setValue(values.get("sourceID"));
        forTwoDirectionCheckBox.setSelected(("-"+values.get("sourceEndValue")).equals(values.get("sourceStartValue")));
        maxSourceValueMod.setText(values.get("sourceEndValue"));
        sourceStepValue.setText(values.get("sourceStep"));
        X_mult.setText(values.get("plotGainX"));
        X_label.setText(values.get("labelX"));
        X_unit.setText(values.get("unitX"));
        measureSelector.setValue(values.get("measureID"));
        Y_mult.setText(values.get("plotGainY"));
        Y_label.setText(values.get("labelY"));
        Y_unit.setText(values.get("unitY"));
        startDelay.setText(values.get("measureDelay"));
        need2PlotCheckBox.setSelected(values.get("needPlot").equals("yes"));
    }
    private boolean checkForm() {
        Boolean result = true;
        //проверка норм
        result&= Methods.checkFloat(X_mult);
        result&=Methods.checkFloat(Y_mult);
        result&=Methods.checkFloat(sourceStepValue);
        result&=Methods.checkFloat(maxSourceValueMod);
        result&=Methods.checkFloat(startDelay);
        return result;
    }

    public void sendSources(Object[] powerSources) {
        String id = "";
        sourceSelector.getItems().clear();
        measureSelector.getItems().clear();
        for (Object sourceID:powerSources){
            id = (String) sourceID;
            sourceSelector.getItems().add(id);
            measureSelector.getItems().add(id);
        }
        sourceSelector.setValue(sourceSelector.getItems().get(0));
        measureSelector.setValue(measureSelector.getItems().get(0));
    }
}
