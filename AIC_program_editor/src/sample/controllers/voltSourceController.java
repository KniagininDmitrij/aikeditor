package sample.controllers;

import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.scene.control.Button;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import sample.Methods;
import sample.PowerSource;
import sample.simple.SourceTimeMode;

public class voltSourceController {

    private String dialog_status = "open";

    private Map<String, String> values = new HashMap<String, String>();

    private PowerSource source;
    public Map<String, String> getValues() {
        return values;
    }
    public String getDialog_status() {
        return dialog_status;
    }

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button dialog_yes_butt;

    @FXML
    private Label dialog_Label;

    @FXML
    private Button dialog_no_butt;

    @FXML
    private TextField voltValueIn;

    @FXML
    private Label textF_label;

    @FXML
    private CheckBox resetCheckBox;

    @FXML
    private ChoiceBox<String> voltUnitsV;

    @FXML
    private ChoiceBox<String> voltRanges;

    @FXML
    private TextField currLimitIn;

    @FXML
    private ChoiceBox<String> currUnitsL;

    @FXML
    private ChoiceBox<String> timeModeList;

    @FXML
    private TextField comment;


    @FXML
    void initialize() {
        initForm();
        dialog_yes_butt.setOnAction(event -> {
            yesEnd();
        });
        dialog_no_butt.setOnAction(event -> {
            dialog_no_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка НЕТ");
            Stage stage = (Stage) dialog_no_butt.getScene().getWindow();
            dialog_status = "no";
            stage.close();
        });
    }

    public void setSource(PowerSource source){
        this.source = source;
        dialog_Label.setText(dialog_Label.getText()+" ("+source.sourceId+")");
        choiceBoxesSetting(source.getVoltageRanges());
    }
    public void setValues(Map<String, String> values) {
        this.values = values;
        if (values.get("status").equals("voltage")){
            resetCheckBox.setSelected(false);
            voltValueIn.setText(values.get("voltage").split(" ")[0]);
            voltUnitsV.setValue(values.get("voltage").split(" ")[1]);
            voltRanges.setValue(values.get("range"));
            currLimitIn.setText(values.get("currentLim").split(" ")[0]);
            currUnitsL.setValue(values.get("currentLim").split(" ")[1]);
            timeModeList.setValue(values.getOrDefault("timeMode", SourceTimeMode.NORMAL.name()));
        }else{
            resetCheckBox.setSelected(true);
        }
        comment.setText(values.get("comment"));
    }
    private void yesEnd() {
        if (checkForm()){
            if (resetCheckBox.isSelected()==false) {
                values.put("status", "voltage");
                values.put("voltage", voltValueIn.getText() + " " + voltUnitsV.getValue());
                values.put("range", voltRanges.getValue());
                values.put("currentLim", currLimitIn.getText() + " " + currUnitsL.getValue());
                values.put("currentLimRange", source.getRangeForValue(values.get("currentLim")));
                values.put("timeMode", timeModeList.getValue());
            }else values.put("status", "reset");
            values.put("comment", comment.getText());

            dialog_yes_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка ДА");
            Stage stage = (Stage) dialog_yes_butt.getScene().getWindow();
            dialog_status = "yes";
            stage.close();
        }
    }

    private void choiceBoxesSetting(String[] voltageRanges) {
        currUnitsL.getItems().clear();
        currUnitsL.getItems().add("uA");
        currUnitsL.getItems().add("mA");
        currUnitsL.getItems().add("A");
        currUnitsL.setValue(currUnitsL.getItems().get(1));
        currUnitsL.show();

        voltRanges.getItems().clear();
        for (String range:voltageRanges){
            voltRanges.getItems().add(range);
        }
        //voltRanges.setValue(voltRanges.getItems().get(0));
        voltRanges.setValue("V");

        voltUnitsV.getItems().clear();
        voltUnitsV.getItems().add("mV");
        voltUnitsV.getItems().add("V");
        voltUnitsV.setValue(voltUnitsV.getItems().get(1));
    }

    boolean checkForm(){
        boolean result = true;
        if (resetCheckBox.isSelected()==false) {
            result &= checkFloatField(voltValueIn, voltUnitsV.getValue(), source.maxVoltage);
            result &= checkFloatField(currLimitIn, currUnitsL.getValue(), source.maxCurrent);
        }
        return result;
    }
    private boolean checkFloatField(TextField field, String unit, Float max) {
        Boolean result = true;
        try {
            float value = Float.parseFloat(field.getText());
            if (unit.length() > 1)
                value *= Methods.getMultSi(unit.substring(0, unit.length() - 1));
            if (value > max) {
                result = false;
                field.setText("Много!");
            }//обязательно сделать в зависимости от
        } catch (Exception e) {
            field.setText("Ошибка!");
            result = false;
        }
        return result;
    }

    private void initForm(){
        timeModeList.getItems().clear();
        Arrays.stream(SourceTimeMode.values()).forEach(t -> timeModeList.getItems().add(t.name()));
        timeModeList.setValue(SourceTimeMode.NORMAL.name());
    }
}
