package sample.controllers;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class dataInputController {
    private String dialog_status = "open";
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button dialog_yes_butt;

    @FXML
    private Label dialog_Label;

    @FXML
    private Button dialog_no_butt;

    @FXML
    private TextField productName;

   /*@FXML
    private TextField adapterCode;*/

    @FXML
    private TextField developerName;

    @FXML
    private TextField chipSizeX;

    @FXML
    private TextField chipSizeY;

    @FXML
    void initialize() {
        dialog_yes_butt.setOnAction(event -> {
            yesEnd();
        });
        dialog_no_butt.setOnAction(event -> {
            dialog_no_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка НЕТ");
            Stage stage = (Stage) dialog_no_butt.getScene().getWindow();
            dialog_status = "no";
            stage.close();
        });
    }

    private void yesEnd() {
        if (checkForm()){



            dialog_yes_butt.setStyle("-fx-background-color: #fafafa");
            System.out.println("В диалоге нажата кнопка ДА");
            Stage stage = (Stage) dialog_yes_butt.getScene().getWindow();
            dialog_status = "yes";
            stage.close();
        }
    }


    boolean checkForm(){
        boolean result = true;
        //result &= checkIntField(adapterCode);
        result &= checkIntField(chipSizeX);
        result &= checkIntField(chipSizeY);
        return result;
    }
    private boolean checkIntField(TextField field) {
        Boolean result = true;
        try {
            float value = Integer.parseInt(field.getText());
        } catch (Exception e) {
            field.setText("Ошибка!");
            result = false;
        }
        return result;
    }

    public String getDialog_status() {
        return dialog_status;
    }

    public void sendPreviousData(Map<String, String> currentInputData) {
        for (Map.Entry<String,String> set: currentInputData.entrySet()){
            switch (set.getKey()){
                case "productName":
                    productName.setText(set.getValue());
                    break;
                case "developerName":
                    developerName.setText(set.getValue());
                    break;
                /*case "adapterCode":
                    adapterCode.setText(set.getValue());
                    break;*/
                case "chipSizeX":
                    chipSizeX.setText(set.getValue());
                    break;
                case "chipSizeY":
                    chipSizeY.setText(set.getValue());
                    break;
            }
        }
    }

    public Map<String, String> getDataIn() {
        Map<String, String> DataIn = new HashMap<String, String>()
        {
            {
                put("productName", productName.getText());
                put("developerName", developerName.getText());
                //put("adapterCode", adapterCode.getText());
                put("chipSizeX", chipSizeX.getText());
                put("chipSizeY", chipSizeY.getText());
            };
        };
        return DataIn;
    }
}
