package sample.controllers;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.List;
import java.util.logging.Logger;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.w3c.dom.Document;
import sample.*;
import sample.enums.DialogEndStatus;
import sample.external.ExcelWorker;
import sample.external.workWithXMLStructure;
import sample.simple.PathHolder;
import sample.simple.ResultParamKey;

public class execPrgController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button save_butt;

    @FXML
    private Label dialog_Label;

    @FXML
    private Button close_butt;

    @FXML
    private ListView<String> availableList;

    @FXML
    private ListView<String> execList;

    @FXML
    private volatile TableView<Result> resultTable;

    @FXML
    private MenuBar execMenu;

    @FXML
    private Button moveUp;

    @FXML
    private Button moveDown;

    @FXML
    private Button ExecCountInc;

    @FXML
    private Button ExecCountDec;

    @FXML
    private volatile Button runButt;
    @FXML
    private volatile Button clearButt;

    private final String defaultExecCountString = "#### | ";
    private final String fieldStringForExecNum = "####";
    private final String pythonScriptPath = "C:\\TestInstruments\\AIK\\Scripts\\run_from_editor.py";
    private final String[] colNamesOfResultTable = new String[]{"№ яч.", "Заголовок", "Параметр", "мин. Нормы", "макс. Нормы", "Измер.", "ед.изм.", "время/дата"};
    private final double[] colWidthsOfResultTable = new double[]{50.0, 100.0, 100.0, 70, 70, 160, 50, 120.0};
    private static final String HEADER_PARAM_SPLITTER = "\\s*:\\s*";
    public static final String WARN_CELL_STYLE = "-fx-background-color:yellow; -fx-text-fill: black;";
    public static final String ERROR_CELL_STYLE = "-fx-background-color:lightcoral; -fx-text-fill: black;";
    public static final String DEFAULT_CELL_STYLE = "";

    private Logger Log;
    private List<Thread> executeSequence = new ArrayList<>();
    TestRunner testRunner;
    TableWriter tableWriter;
    private int programIndex;
    private boolean isRunNow = false;
    private boolean needStopExecution = false;
    private int maxEqualParamResultCountPerCurrentTable;
    private Map<ResultParamKey, List<Result>> equalParamResultsMap = new HashMap<>();
    private List<ResultParamKey> resultMapSortedKey = new ArrayList<>();
    private Set<String> originBlockNames = new HashSet<>();
    private List<String> sortedOriginBlockNames = new ArrayList<>();

    @FXML
    void initialize() {
        initMenus();
        initResultTable();
        setSelectModelAvailableList();
        setSelectModelExecList();
        initExecListButts();
        initBottomButtFunctions();
    }

    public void setNeedStopExecution(boolean needStopExecution) {
        this.needStopExecution = needStopExecution;
    }

    public void setInputData(String libPath, String fileName){
        fillAvailableListByLibPath(libPath);
        addFile2ExecList(fileName);
        checkAndRunCurrentList();
    }

    private void initBottomButtFunctions() {
        runButt.setOnMouseClicked(event -> {
            if (!isRunNow){
                checkAndRunCurrentList();
            } else{
                try {
                    Methods.messageDialog("Измерения уже запущены! Для перехода в паузу нажмите клавишу ESC, для выхода из паузы ENTER, для останова DELETE.");
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        });
        close_butt.setOnMouseClicked(event -> {
            closeExecuteWindow();
        });
        save_butt.setOnMouseClicked(event -> {
            tryExport2Excel();
        });
        clearButt.setOnMouseClicked(event -> {
            if (!isRunNow){
                clearResultsTable();
            }else {
                waitExecuteEndUserDialog();
            }
        });
    }

    private void checkAndRunCurrentList() {
        //1 - check execList
        if (isExecListValid()){
            executeSequence.clear();
            for (int i = 0; i<execList.getItems().size();i++){
                int finalI = i;
                executeSequence.add(new Thread(()->{
                    String execListItem = execList.getItems().get(finalI);
                    String fileName = getFileNameFromExecListItem(execListItem);
                    int runCount = getExecCountFromStringOfExecListItemOr0(execListItem);
                    if (!runOneProgram(fileName, runCount)){
                        try {
                            Methods.messageDialog("Тестирование остановлено из-за ошибок");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }));
            }
            startExecuting();
        }else{
            try {
                if (availableList.getItems().size()>0){
                    Methods.messageDialog("Выберете программы для запуска из списка слева!");
                }else{
                    Methods.messageDialog("Выберете папку с необходимыми программами!");
                }
            }catch (IOException e){

            }
        }
    }

    private void startExecuting() {
        runButt.setStyle("-fx-background-color: #999;");
        isRunNow = true;
        programIndex = 0;
        executeSequence.get(programIndex).start();
    }

    public void runNextProgram(){
        programIndex++;
        if (programIndex < executeSequence.size() && !needStopExecution){
            executeSequence.get(programIndex).start();
            return;
        }
        runButt.setStyle("-fx-background-color: #fff; -fx-border-color: black; -fx-border-radius: 5px;");
        isRunNow = false;
    }

    private boolean runOneProgram(String fileName, int runCount) {
        String fullPathForProgramFileName = getFullPathForProgramFileName(fileName);
        List<String> paramNamesList = new ArrayList();
        String countStr = Integer.toString(runCount);
        boolean hasNames = true;
        try {
            String fullPathForProgramFileNameWithoutQuotes = fullPathForProgramFileName.replaceAll("\"", "");
            Document document = new workWithXMLStructure().createNewXMLDocument(fullPathForProgramFileNameWithoutQuotes);
            paramNamesList = Methods.getParamNamesForResults(document);
            runCount *= paramNamesList.size();
        } catch (Exception e) {
            hasNames = false;
        }
        String Debug = "0";
        try {
            ResultParser.init();
        } catch (IOException | InterruptedException exception) {
            exception.printStackTrace();
        }
        testRunner = new TestRunner(pythonScriptPath, new String[]{fullPathForProgramFileName, Debug, countStr}, Log, fileName);
        new Thread(testRunner).start();

        tableWriter = new TableWriter(runCount, paramNamesList, hasNames, fileName, Log, this, testRunner);
        new Thread(tableWriter).start();
        return true;
    }


    public void addRow2ResultTable(String headerTextAndParamName, Result result){
        String blockName = headerTextAndParamName.split(HEADER_PARAM_SPLITTER)[2];
        String paramName = headerTextAndParamName.split(HEADER_PARAM_SPLITTER)[1];
        String headerText = headerTextAndParamName.split(HEADER_PARAM_SPLITTER)[0];
        result.setBlockName(blockName);
        result.setParamName(paramName);
        result.setHeaderText(headerText);
        upDateEqualParamResultsMap(result);
        resultTable.getItems().add(result);
    }

    private void upDateEqualParamResultsMap(Result result) {
        ResultParamKey resultParamKey = new ResultParamKey(result);
        List equalParamResults = equalParamResultsMap.getOrDefault(resultParamKey, new ArrayList<Result>());
        if (equalParamResults.size() == 0){
            resultMapSortedKey.add(resultParamKey);
        }
        String blockName = resultParamKey.getBlockName();
        if (!originBlockNames.contains(blockName)){
            originBlockNames.add(blockName);
            sortedOriginBlockNames.add(blockName);
        }
        equalParamResults.add(result);
        equalParamResultsMap.put(resultParamKey, equalParamResults);

        int countOfEqualParamResults = equalParamResults.size();
        if (countOfEqualParamResults > maxEqualParamResultCountPerCurrentTable){
            maxEqualParamResultCountPerCurrentTable = countOfEqualParamResults;
        }
    }

    private String getFullPathForProgramFileName(String fileName) {
        String fullPath = PathHolder.getExecPath() + "\\" + fileName;
        if (fullPath.contains(" ")){
            fullPath = "\"" + fullPath + "\"";
        }
        return fullPath;
    }

    private String getFileNameFromExecListItem(String execListItem) {
        return execListItem.substring(defaultExecCountString.length());
    }

    private boolean isExecListValid() {
        return execList.getItems().size() != 0;
    }
    private void customiseFactory(TableColumn<Result, String> calltypel) {
        calltypel.setCellFactory(column -> {
            return new TableCell<Result, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);

                    setText(empty ? "" : getItem().toString());
                    setGraphic(null);

                    TableRow<Result> currentRow = getTableRow();
                    Result result = currentRow.getItem();
                    String rowStyle = DEFAULT_CELL_STYLE;
                    String cellStyle = DEFAULT_CELL_STYLE;
                    if (result!=null) {
                        if(!result.isInNorm()){
                            rowStyle = ERROR_CELL_STYLE;
                            cellStyle = ERROR_CELL_STYLE;
                        }
                        if(result.isLastSuccess()){
                            cellStyle = WARN_CELL_STYLE;
                        }
                    }
                    currentRow.setStyle(rowStyle);
                    this.setStyle(cellStyle);
                }
            };
        });
    }
    private void initResultTable(){
        resultTable.getColumns().clear();
        clearResultsTable();

        TableColumn numberColumn = new TableColumn(colNamesOfResultTable[0]);
        numberColumn.setPrefWidth(colWidthsOfResultTable[0]);
        customiseFactory(numberColumn);
        numberColumn.setCellValueFactory(new PropertyValueFactory<>("number"));

        TableColumn headerColumn = new TableColumn(colNamesOfResultTable[1]);
        headerColumn.setPrefWidth(colWidthsOfResultTable[1]);
        headerColumn.setCellValueFactory(new PropertyValueFactory<>("headerText"));

        TableColumn paramColumn = new TableColumn(colNamesOfResultTable[2]);
        paramColumn.setPrefWidth(colWidthsOfResultTable[2]);
        paramColumn.setCellValueFactory(new PropertyValueFactory<>("paramName"));

        TableColumn minNormColumn = new TableColumn(colNamesOfResultTable[3]);
        minNormColumn.setPrefWidth(colWidthsOfResultTable[3]);
        minNormColumn.setCellValueFactory(new PropertyValueFactory<>("minNorm"));

        TableColumn maxNormColumn = new TableColumn(colNamesOfResultTable[4]);
        maxNormColumn.setPrefWidth(colWidthsOfResultTable[4]);
        maxNormColumn.setCellValueFactory(new PropertyValueFactory<>("maxNorm"));

        TableColumn measureColumn = new TableColumn(colNamesOfResultTable[5]);
        measureColumn.setPrefWidth(colWidthsOfResultTable[5]);
        measureColumn.setCellValueFactory(new PropertyValueFactory<>("measure"));

        TableColumn unitColumn = new TableColumn(colNamesOfResultTable[6]);
        unitColumn.setPrefWidth(colWidthsOfResultTable[6]);
        unitColumn.setCellValueFactory(new PropertyValueFactory<>("unit"));

        TableColumn dateTimeColumn = new TableColumn(colNamesOfResultTable[7]);
        dateTimeColumn.setPrefWidth(colWidthsOfResultTable[7]);
        dateTimeColumn.setCellValueFactory(new PropertyValueFactory<>("dateTime"));

        resultTable.getColumns().addAll(numberColumn, headerColumn, paramColumn, minNormColumn, maxNormColumn, measureColumn, unitColumn, dateTimeColumn);
    }

    private void clearResultsTable() {
        resultTable.getItems().clear();
        Result.setCounter(0);
        maxEqualParamResultCountPerCurrentTable = 0;
        equalParamResultsMap.clear();
        resultMapSortedKey.clear();
        originBlockNames.clear();
        sortedOriginBlockNames.clear();
    }

    private void initMenus(){
        execMenu.getMenus().clear();
        Menu fileMenu = new Menu("Файл");
        fileMenu.getItems().addAll(
                new MenuItem("Выбрать папку"));
        fileMenu.setId("#fileMenu");
        execMenu.getMenus().add(fileMenu);
        fileMenu.setOnAction(event -> {
            fillAvailableListByLibPath();
        });
    }
    private void initExecListButts(){
        moveUp.setOnMouseClicked(event -> {
            setMoveUp();
        });
        moveDown.setOnMouseClicked(event -> {
            setMoveDown();
        });
        ExecCountDec.setOnMouseClicked(event -> {
            changeExecCountForSelectedItem(-1);
        });
        ExecCountInc.setOnMouseClicked(event -> {
            changeExecCountForSelectedItem(1);
        });
        execList.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle (KeyEvent event){
                if (event.getCode() == KeyCode.DELETE) {
                    deleteSelectedItemFromExecList();
                }
            }
        });
        execList.getParent().setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle (KeyEvent event){
                if (event.getCode() == KeyCode.DELETE) {
                    setNeedStopExecution(true);
                    tableWriter.stop();
                }
            }
        });

    }
    private void initAvailablePrgList(ArrayList<String> libFileList) {
        availableList.getItems().clear();
        ListIterator iter = libFileList.listIterator();
        while (iter.hasNext()){
            availableList.getItems().add(iter.next().toString());
        }
    }

    private void fillAvailableListByLibPath() {
        PathHolder.setExecPath(Methods.getPath2library(PathHolder.getExecPath()));
        ArrayList<String> libFileList = Methods.listFilesForFolder(new File(PathHolder.getExecPath()), ".xml");
        initAvailablePrgList(libFileList);
    }

    private void fillAvailableListByLibPath(String libPath) {
        PathHolder.setExecPath(libPath);
        ArrayList<String> libFileList = Methods.listFilesForFolder(new File(libPath), ".xml");
        initAvailablePrgList(libFileList);
    }

    private void setSelectModelAvailableList() {
        availableList.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent click) {
                if (click.getClickCount() == 2) {
                    addFile2ExecList(availableList.getSelectionModel().getSelectedItem());
                }
            }
        });
    }

    private void addFile2ExecList(String fileName) {
        String value = getExecCountStringForIntNum(1)+fileName;
        execList.getItems().add(value);
        execList.getSelectionModel().select(execList.getItems().size()-1);
    }

    private void setSelectModelExecList() {
        execList.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent click) {
                if (click.getClickCount() == 2) {
                    int oldCount = getExecCountFromStringOfExecListItemOr0(execList.getSelectionModel().getSelectedItem());
                    int newCount = oldCount;
                    try {
                        String  newCountString = Methods.twoStateDialogWithTextFieldDef("Введите количество запусков:", "Запустить [раз]:",
                                Integer.toString(oldCount),"int");
                        newCount = Integer.parseInt(newCountString);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    changeExecCountForSelectedItem(newCount-oldCount);
                }
            }
        });
    }

    private void deleteSelectedItemFromExecList() {
        if (execList.getItems().size() == 0) return;
        int selectedIndex = execList.getSelectionModel().getSelectedIndex();
        execList.getItems().remove(selectedIndex);
    }

    private void changeExecCountForSelectedItem(int delta) {
        if (execList.getItems().size() == 0) return;
        String selectedExecItem = execList.getSelectionModel().getSelectedItem();
        int selectedExecItemIndex = execList.getSelectionModel().getSelectedIndex();
        int currentExecCount = getExecCountFromStringOfExecListItemOr0(selectedExecItem);
        if ((delta<0 && currentExecCount<=1) || (delta>0 && currentExecCount==999)) return;
        String newStringForSelectedItem = getExecCountStringForIntNum(currentExecCount+delta)+selectedExecItem.substring(defaultExecCountString.length());
        execList.getItems().set(selectedExecItemIndex, newStringForSelectedItem);
        execList.getSelectionModel().select(selectedExecItemIndex);
    }

    private void setMoveUp(){
        if (execList.getItems().size() == 0) return;
        int selectedIndex = execList.getSelectionModel().getSelectedIndex();
        String selectedValue = execList.getSelectionModel().getSelectedItem();
        if (selectedIndex>0){
            String upperValue = execList.getItems().get(selectedIndex-1);
            execList.getItems().set(selectedIndex, upperValue);
            execList.getItems().set(selectedIndex-1, selectedValue);
            execList.getSelectionModel().select(selectedIndex-1);
        }
    }

    private void setMoveDown(){
        if (execList.getItems().size() == 0) return;
        int selectedIndex = execList.getSelectionModel().getSelectedIndex();
        String selectedValue = execList.getSelectionModel().getSelectedItem();
        if (selectedIndex<(execList.getItems().size()-1)){
            String lowerValue = execList.getItems().get(selectedIndex+1);
            execList.getItems().set(selectedIndex, lowerValue);
            execList.getItems().set(selectedIndex+1, selectedValue);
            execList.getSelectionModel().select(selectedIndex+1);
        }
    }

    private int getExecCountFromStringOfExecListItemOr0(String ExecListItem){
        if (ExecListItem == null) return 0;
        int ExecCount = 1;
        int StartIndex = defaultExecCountString.indexOf(fieldStringForExecNum);
        int EndIndex = StartIndex+fieldStringForExecNum.length();
        String NumString = ExecListItem.substring(StartIndex,EndIndex);
        try {
            ExecCount = Integer.parseInt(NumString.trim());
        }catch (Exception e){

        }
        return ExecCount;
    }

    private String getExecCountStringForIntNum(int ValidNum){
        String result = defaultExecCountString;
        String NumString = Integer.toString(ValidNum);
        while(NumString.length()<fieldStringForExecNum.length()){
            NumString = " "+NumString;
        }
        return result.replaceFirst(fieldStringForExecNum, NumString);
    }

    public void closeExecuteWindow() {
        if (isRunNow){
            waitExecuteEndUserDialog();
            return;
        }
        if (isResultTableNotEmpty()){
            try{
                if (Methods.twoStateDialog("Сохранить отчет о результатах?") == DialogEndStatus.YES){
                    tryExport2Excel();
                }
            }catch (IOException e){

            }
        }
        Stage stage = (Stage) close_butt.getScene().getWindow();
        stage.close();
    }

    private void tryExport2Excel() {
        if (isResultTableNotEmpty()) {
            try {
                String excelFilePath = Methods.getPath2saveNewFile(PathHolder.getExecPath(), "xlsx");
                if (ExcelWorker.exportFromTableViewItems(excelFilePath, equalParamResultsMap, resultMapSortedKey,
                        sortedOriginBlockNames, colNamesOfResultTable, maxEqualParamResultCountPerCurrentTable)){
                    Methods.messageDialog("Отчет сохранен!");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            try {
                Methods.messageDialog("Таблица измерений пуста! Запустите испытание.");
            }catch (IOException e){

            }
        }
    }

    private boolean isResultTableNotEmpty(){
         return resultTable.getItems()!=null && resultTable.getItems().size()>0;
    }

    public void setLog(Logger log) {
        Log = log;
    }

    private void waitExecuteEndUserDialog() {
        try {
            Methods.messageDialog("Дождитесь окончания выполнения текущей программы!");
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public void setLastSuccess() {
        try {
            resultTable.getItems().get(resultTable.getItems().size() - 1).setLastSuccess(true);
            resultTable.refresh();
        }catch (Exception exception){}
    }
}

