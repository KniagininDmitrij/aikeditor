package sample;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class ClipBoardDrive {
    private  static Element ROOT_ELEMENT_BUFFER;
    private final static Document BUFFER_DOCUMENT;
    private final static Clipboard CLIPBOARD;
    private final static String ENCODE_NAME = "UTF-8";
    private final static String ROOT_ELEMENT_NAME = "clipBoard";
    private static List<Node> Nodes = new ArrayList();

    static {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        BUFFER_DOCUMENT = builder.newDocument();
        ROOT_ELEMENT_BUFFER = BUFFER_DOCUMENT.createElement(ROOT_ELEMENT_NAME);
        BUFFER_DOCUMENT.appendChild(ROOT_ELEMENT_BUFFER);
        CLIPBOARD = Toolkit.getDefaultToolkit().getSystemClipboard();
    }

    public static void sendItemsToClipBoard(List<Node> items){
        String strItems = getStringForNodes(items);
        StringSelection selection = new StringSelection(strItems);
        CLIPBOARD.setContents(selection, selection);
    }

    public static List<Node> getItemsFromClipBoard()
            throws IOException, ParserConfigurationException, SAXException, UnsupportedFlavorException {
        String bufferString = (String) CLIPBOARD.getData(DataFlavor.stringFlavor);
        return tryParseToNodes(bufferString);
    }

    private static String getStringForNodes(List<Node> nodes){
        BUFFER_DOCUMENT.removeChild(ROOT_ELEMENT_BUFFER);
        ROOT_ELEMENT_BUFFER = BUFFER_DOCUMENT.createElement(ROOT_ELEMENT_NAME);
        BUFFER_DOCUMENT.appendChild(ROOT_ELEMENT_BUFFER);
        for(Node node : nodes){
            ROOT_ELEMENT_BUFFER.appendChild(
                    BUFFER_DOCUMENT.adoptNode(node.cloneNode(true)));
        }

        return toString(BUFFER_DOCUMENT);
    }

    private static String toString(Document doc) {
        try {
            StringWriter sw = new StringWriter();
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, ENCODE_NAME);

            transformer.transform(new DOMSource(doc), new StreamResult(sw));
            return sw.toString();
        } catch (Exception ex) {
            throw new RuntimeException("Error converting to String", ex);
        }
    }

    private static List<Node> tryParseToNodes(String clipBoardStr) throws ParserConfigurationException, IOException, SAXException {
        Element clipRoot =  DocumentBuilderFactory
                .newInstance()
                .newDocumentBuilder()
                .parse(new ByteArrayInputStream(clipBoardStr.getBytes(Charset.forName(ENCODE_NAME))))
                .getDocumentElement();
        if (!clipRoot.getNodeName().equals(ROOT_ELEMENT_NAME)){
            throw new IllegalArgumentException("Pasted string not contain " + ROOT_ELEMENT_NAME + " element");
        }
        NodeList nodes = clipRoot.getChildNodes();
        List<Node> nodeList = new ArrayList<>();
        for (int i = 0; i < nodes.getLength(); i++){
            Node node = nodes.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE){
                nodeList.add(node);
            }
        }
        return nodeList;
    }
}

