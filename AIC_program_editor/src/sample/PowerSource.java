package sample;

import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.*;

public class PowerSource {
    public String sourceId;
    public Button VSourceIcon;
    public Button VMeasIcon;
    public Button ISourceIcon;
    public Button IMeasIcon;
    public Float maxVoltage;
    public Float maxCurrent;

    private String model;
    private String currentFunction;
    private Float outputCurrent;
    private String currUnit;
    private String currRange;
    private Float currLimit;
    private String currLimUnit;

    private Float outputVoltage;
    private String voltUnit;
    private String voltRange;
    private Float voltLimit;
    private String voltLimUnit;

    private static final String[] SMU4139_currentRanges =  new String[] {"1 uA","10 uA","100 uA","1 mA","10 mA","100 mA","1 A","3 A"};
    private static final String[] SMU4137_currentRanges =  new String[] {"1 uA","10 uA","100 uA","1 mA","10 mA","100 mA","1 A"};
    private static final String[] SMU4139_voltageRanges =  new String[] {"600 mV","6 V","60 V"};
    private static final String[] SMU4137_voltageRanges =  new String[] {"600 mV","6 V","20 V","200 V"};

    PowerSource(String id, String model, Float maxV, Float maxI){
        this.sourceId = id;
        this.maxVoltage = maxV;
        this.maxCurrent = maxI;
        this.model = model;
    }

    public String[] getCurrentRanges(){
        switch (model){
            case "PXIe-4139":
                return SMU4139_currentRanges;
            case "PXIe-4137":
                return SMU4137_currentRanges;
            default:
                return new String[]{};
        }
    }

    public String[] getVoltageRanges(){
        switch (model){
            case "PXIe-4139":
                return SMU4139_voltageRanges;
            case "PXIe-4137":
                return SMU4137_voltageRanges;
            default:
                return new String[]{};
        }
    }
    public void visibleSelectionIcon(boolean b, String type ){
        switch (type){
            case "voltMeas":
                buttSetter(b,VMeasIcon);
                break;
            case "voltageSource":
                buttSetter(b,VSourceIcon);
                break;
            case "currentSource":
                buttSetter(b,ISourceIcon);
                break;
            case "currMeas":
                buttSetter(b,IMeasIcon);
                break;
        }
    }

    private void buttSetter(boolean b, Button button) {
        if (b){
            button.setStyle("-fx-background-color: fff;-fx-border-color: red; -fx-border-width: 2;");
        }else{
            button.setStyle("-fx-background-color: fff;-fx-border-color: silver; -fx-border-width: 1;");
        }
    }

    public String getRangeForValue(String value){
     String unit = value.substring(value.length()-1);
     String[] ranges = new String[]{};
     String defaultRes = "";
     switch (this.model+unit){
         case "PXIe-4139A":
                ranges = SMU4139_currentRanges;
                defaultRes = "3 A";
             break;
         case "PXIe-4139V":
             ranges = SMU4139_voltageRanges;
             defaultRes = "60 V";
             break;
         case "PXIe-4137A":
             ranges = SMU4137_currentRanges;
             defaultRes = "1 A";
             break;
         case "PXIe-4137V":
             ranges = SMU4137_voltageRanges;
             defaultRes = "200 V";
             break;
     }
     for (String range:ranges){
        if (Methods.getValueFromStringWithUnit(range)>=Methods.getValueFromStringWithUnit(value))
            return range;
     }
     return defaultRes;
    }
    private void imgSetter(boolean b, ImageView imgView, String imgPathTrue, String imgPathFalse){
        imgView.setImage(new Image(Main.class.getResourceAsStream((b?imgPathTrue:imgPathFalse))));
    }
    public void setCurrentSource(){}

    public void setCurrentMeas(){}

    public void setVoltageSource(){}

    public void setVoltageMeas(){}

    public void toInit() {
        //VSourceIcon.setImage(new Image(Main.class.getResourceAsStream("images/VSourceImg.png")));
        buttSetter(false,VMeasIcon);
        buttSetter(false,VSourceIcon);
        buttSetter(false,ISourceIcon);
        buttSetter(false,IMeasIcon);
    }
}
