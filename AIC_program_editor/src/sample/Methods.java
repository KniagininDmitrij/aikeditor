package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TreeItem;
import java.awt.Image;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.TextField;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import sample.controllers.YN_with_textIn_controller;
import sample.controllers.dialog_controller;
import sample.controllers.execPrgController;
import sample.controllers.message_controller;
import sample.enums.DialogEndStatus;
import sample.external.workWithXMLStructure;

import javax.imageio.ImageIO;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.Exchanger;
import java.util.logging.Logger;

public class Methods {
    private static final String ERROR_DESCRIPTION_START_STRING = "Error: ";
    private static final String ERROR_MESSAGE_START_STRING = "Ошибка при исполнении \"%s\": ";

    public static float getMultSi(String prefix){
        switch (prefix){
            case "n": case "н":
                return 1.0E-9F;
            case "u": case "мк":
                return 1.0E-6F;
            case "m": case "м":
                return 1.0E-3F;
            case "k": case "к":
                return 1.0E3F;
            case "M": case "М":
                return 1.0E6F;
            case "G": case "Г":
                return 1.0E9F;
        }
        return 1F;
    }
    public static float getValueFromStringWithUnit(String value){
        String withoutUnit = value.split(" ")[0];
        String unit = value.split(" ")[1];
        unit = unit.substring(0,unit.length()-1);
        try{
            return Float.parseFloat(withoutUnit)*getMultSi(unit);
        }catch (Exception e){
            System.out.println("Methods getValueFromStringWithUnit Error: "+e.fillInStackTrace());
        }
        return 0;
    }
    public static boolean isEqualTreeNodes(TreeItem<String> first, TreeItem<String> second) {
        boolean result = true;
        if (first.getValue().equals("Результат:") && second.getValue().equals("Результат:")){
            ListIterator<TreeItem<String>> iterFirst = first.getChildren().listIterator();
            ListIterator<TreeItem<String>> iterSecond = second.getChildren().listIterator();
            while (iterFirst.hasNext() && iterFirst.hasNext() && result) {
                try {
                    result = iterFirst.next().equals(iterSecond.next());
                }catch (NoSuchElementException noSuchElementException){
                    result = false;
                }
            }
        }else{
            result = first.equals(second);
        }
        return result;
    }
    public static String getAtrByName(Node node, String name){
        return node.getAttributes().getNamedItem(name).getNodeValue();
    }

    public static DialogEndStatus twoStateDialog(String message) throws IOException {
        Stage dialog = new Stage();
        FXMLLoader loader = new FXMLLoader(Methods.class.getResource("views/yes_or_no.fxml"));
        Parent root = loader.load();


        dialog.setTitle(message);
        dialog.setScene(new Scene(root, 350, 200));
        // populate dialog with controls.

        //dialog.initOwner(parentStage);
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog_controller controller = loader.<dialog_controller>getController();
        controller.setMessage(message);
        dialog.showAndWait();
        //получаем статус завершения диалога с пользователем
        System.out.println(controller.getDialogStatus());
        return controller.getDialogStatus();
    }
    public static String twoStateDialogWithTextFieldDef(String message, String textFieldLabel,  String inputDef, String inputType) throws IOException {
        Stage dialog = new Stage();
        FXMLLoader loader = new FXMLLoader(Methods.class.getResource("views/YN_with_textIn.fxml"));
        Parent root = loader.load();


        dialog.setTitle(message);
        dialog.setScene(new Scene(root, 350, 200));
        // populate dialog with controls.

        //dialog.initOwner(parentStage);
        dialog.initModality(Modality.APPLICATION_MODAL);
        YN_with_textIn_controller controller = loader.<YN_with_textIn_controller>getController();
        controller.setContent(message, textFieldLabel, inputDef, inputType);
        dialog.showAndWait();
        //получаем статус завершения диалога с пользователем
        if (controller.getDialog_status() != DialogEndStatus.YES)
            return inputDef;
        return (controller.getInputText());
    }
    public static boolean messageDialog(String message) throws IOException {
        Stage dialog = new Stage();
        FXMLLoader loader = new FXMLLoader(Methods.class.getResource("views/message.fxml"));
        Parent root = loader.load();

        dialog.setTitle(message);
        dialog.setScene(new Scene(root, 350, 200));
        dialog.initModality(Modality.APPLICATION_MODAL);
        message_controller controller = loader.<message_controller>getController();
        controller.setMessage(message);
        dialog.showAndWait();
        //получаем статус завершения диалога с пользователем
        System.out.println(controller.getDialogStatus());
        return (controller.getDialogStatus()=="yes"?true:false);
    }

    public static boolean checkFloat(String SomeText){
        try {
            Float.parseFloat(SomeText);
            return true;
        }catch (Exception e){
            return false;
        }
    }
    public static Boolean checkInt(TextField textField) {
        try {
            int num = Integer.parseInt(textField.getText());
            return true;
        }catch (Exception exception){
            textField.setText("Ошибка");
            return false;
        }
    }

    public static boolean checkFloat(TextField TF){
        try {
            Float.parseFloat(TF.getText());
            return true;
        }catch (Exception e){
            TF.setText("Ошибка!");
            return false;
        }
    }

    public static ArrayList<String> listFilesForFolder(final File folder, String extension) {
        ArrayList<String> fileList = new ArrayList<String>();
        for (final File fileEntry : folder.listFiles()) {
            if (!fileEntry.isDirectory()) {
                String fileName = fileEntry.getName();
                if (fileName.contains(extension)){
                    System.out.println(fileName);
                    fileList.add(fileName);
                }
            }
        }
        return fileList;
    }

    public static String getPath2library(String libPath) {
        DirectoryChooser chooser = new DirectoryChooser();
        if (libPath!=null && !libPath.equals("no path")){
            chooser.setInitialDirectory(new File(libPath));
        }
        File dir = chooser.showDialog(new Stage());
        if (dir != null) {
            libPath = dir.getAbsolutePath();
            return libPath;
        }
        return "no path";
    }

    public static String  getPath2saveNewFile(String defaultPath, String extension){
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().addAll(//
                new FileChooser.ExtensionFilter(extension.toUpperCase(), "*."+extension.toLowerCase()),
                new FileChooser.ExtensionFilter("All Files", "*.*"));
        chooser.setInitialDirectory(new File(defaultPath));

        File dir = chooser.showSaveDialog(new Stage());
        if (dir != null) {
            return dir.getAbsolutePath();
        }
        return defaultPath;
    }

    public static ChoiceBox<String> getVariantsCB(String[] strings) {
        ChoiceBox<String> CB = new ChoiceBox<String>();
        for (String v:strings){
            CB.getItems().add(v);
        }
        return CB;
    }
    public static void runExecPrgController(String libPath, String fileName, Logger Log) throws IOException {
        Stage dialog = new Stage();
        FXMLLoader loader = new FXMLLoader(Methods.class.getResource("views/executeWindow.fxml"));
        Parent root = loader.load();

        dialog.setScene(new Scene(root, 1250, 680));
        dialog.initModality(Modality.APPLICATION_MODAL);
        execPrgController controller = loader.<execPrgController>getController();
        controller.setInputData(libPath, fileName);
        controller.setLog(Log);
        dialog.setOnCloseRequest(event -> {
            controller.closeExecuteWindow();
        });
        dialog.showAndWait();
        //получаем статус завершения диалога с пользователем
    }
    public static void runExecPrgController(Logger Log) throws IOException {
        Stage dialog = new Stage();
        FXMLLoader loader = new FXMLLoader(Methods.class.getResource("views/executeWindow.fxml"));
        Parent root = loader.load();

        dialog.setScene(new Scene(root, 1250, 680));
        dialog.initModality(Modality.APPLICATION_MODAL);
        execPrgController controller = loader.<execPrgController>getController();
        controller.setLog(Log);
        dialog.setOnCloseRequest(event -> {
            controller.closeExecuteWindow();
        });
        dialog.showAndWait();
        //получаем статус завершения диалога с пользователем
    }
    public static String getNearHeaderTextForNodeFromHeaderList(Node node, NodeList headersList){
        String result = "";
        for (int headerIndex = headersList.getLength()-1; headerIndex >= 0; headerIndex--){
            if (headersList.item(headerIndex).compareDocumentPosition(node) == Node.DOCUMENT_POSITION_FOLLOWING){
                result = headersList.item(headerIndex).getTextContent();
                break;
            }
        }
        return result;
    }

    public static List<String> getParamNamesForResults(Document document) {
        List<String> names = new ArrayList<>();
        Node streamNode = document.getElementsByTagName("stream").item(0);
        NodeList streamNodes = streamNode.getChildNodes();
        for(int i = 0; i < streamNodes.getLength(); i++){
            Node streamChildNode = streamNodes.item(i);
            boolean correctNode = streamChildNode.getNodeName().equals("result") ||
                    streamChildNode.getNodeName().equals("Vac");
            if (correctNode){
                try {
                    //because only in teg stream result have this attribute
                    String blockName = "reSaveFilePlease";
                    if (streamChildNode.getAttributes().getNamedItem("paramName") != null)
                        blockName = streamChildNode.getAttributes().getNamedItem("blockName").getNodeValue();
                    if (blockName == null){blockName = "unknownBlock";}
                    String data = streamChildNode.getAttributes().getNamedItem("headerText").getNodeValue() + " : "
                            + streamChildNode.getAttributes().getNamedItem("paramName").getNodeValue()+ " : "
                            + blockName;
                    if (streamChildNode.getNodeName().equals("Vac")){
                        int vacPointCount = getVacPointCount(streamChildNode);
                        for (int j = 0; j < vacPointCount; j++){
                            names.add(data);
                        }
                        continue;
                    }
                    names.add(data);

                }catch (NullPointerException ex){
                    continue;
                }
            }
        }
        return names;
    }

    private static int getVacPointCount(Node vacNode) {
        try{
            double startSourcePoint = Double.parseDouble(
                    vacNode.getAttributes().getNamedItem("sourceStartValue").getNodeValue());
            double endSourcePoint = Double.parseDouble(
                    vacNode.getAttributes().getNamedItem("sourceEndValue").getNodeValue());
            double stepSourcePoint = Double.parseDouble(
                    vacNode.getAttributes().getNamedItem("sourceStep").getNodeValue());
            int pointCount = (int)(Math.abs(endSourcePoint - startSourcePoint) / stepSourcePoint) + 2;
            return pointCount;
        }catch (Exception exception){
            return 0;
        }
    }

    public static String getDateTimeString(){
        LocalDateTime date = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss dd.MM.YY");
        return date.format(formatter);
    }

    public static ArrayList<Integer> getIndexMapForSelectedTreeItem(TreeItem<String> selectedItem) {
        ArrayList<Integer> indexMap = new ArrayList<Integer>();
        ListIterator<TreeItem<String>> iter = selectedItem.getParent().getChildren().listIterator();
        int counter = 0;
        while (!Methods.isEqualTreeNodes(iter.next(),selectedItem)) {
            counter++;
        }
        //Log.info(selectedItem.getValue()+":"+counter);
        indexMap.add(0, counter);
        for (TreeItem<String> item = selectedItem;
             item != null; item = item.getParent()) {
            try {
                iter = item.getParent().getChildren().listIterator();
                counter = 0;
                while (!Methods.isEqualTreeNodes(iter.next(),item)) {
                    counter++;
                }
                //Log.info(item.getValue()+":"+counter);
                indexMap.add(0, counter);
            } catch (NullPointerException e) {
                //Log.info("Нет родителя");
            }
        }
        return indexMap;
    }

    public static Node getNodeFromDocumentByIndexMap(Document programDocument, ArrayList<Integer> indexMap, ProgramObjectClass programObject) {
        NodeList nList = null;
        try {
            nList = programDocument.getChildNodes().item(0).getChildNodes();//new workWithXMLStructure().getNodesFromDocument(programDocument);
        } catch (Exception exception){
            exception.printStackTrace();
        }
        Node currNode = getNodeByIndex(nList, 0, programObject);
        for (int i=0;i<indexMap.size()-1;i++){
            currNode = getNodeByIndex(nList,indexMap.get(i), programObject);
            nList = currNode.getChildNodes();
        }
        return currNode;
    }

    private static Node getNodeByIndex(NodeList nList, int index, ProgramObjectClass programObject){
        int count = 0;
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node node = nList.item(temp);

            if (node.getNodeType() == Node.ELEMENT_NODE && !node.getNodeName().equals("stream")) {
                programObject.refreshGUIObjectsByNode(node, count<index);
                if (count==index) {
                    return node;
                }
                count++;
            }
        }
        return null;
    }

    public static BufferedImage putImageUnderExistImage(String upperImageSrc, String lowerImageSrc, Object objectWithSameResource) throws IOException {
        Image imageParent = ImageIO.read(objectWithSameResource.getClass().getResource(upperImageSrc));
        Image imageChild = ImageIO.read(objectWithSameResource.getClass().getResource(lowerImageSrc));
        BufferedImage newImage = new BufferedImage(imageParent.getWidth(null), imageParent.getHeight(null) + imageChild.getHeight(null),  BufferedImage.TYPE_INT_RGB);
        newImage.getGraphics().drawImage(imageParent, 0, 0, imageParent.getWidth(null), imageParent.getHeight(null), null);
        newImage.getGraphics().drawImage(imageChild, 0, imageParent.getHeight(null), imageChild.getWidth(null), imageChild.getHeight(null), null);
        return newImage;
    }

    public static void reWriteLinkedDoubleBlocksForBlock(Document programDocument, Node block, String oldName, String newName) {
        NodeList doubleBlocks = programDocument.getElementsByTagName("d_block");
        for (int i = 0; i < doubleBlocks.getLength(); i++){
            try {
                Node doubleBlock = doubleBlocks.item(i);
                String prototypeId = doubleBlock.getAttributes().getNamedItem("pro_id").getNodeValue();
                String prototypeName = doubleBlock.getAttributes().getNamedItem("pro_name").getNodeValue();
                String blockId = block.getAttributes().getNamedItem("id").getNodeValue();

                if (prototypeId.equals(blockId) && prototypeName.equals(oldName)){
                    doubleBlock.getAttributes().getNamedItem("pro_name").setNodeValue(newName);
                }
            }catch (NullPointerException nullPointerException){
                continue;
            }
        }
    }
}
