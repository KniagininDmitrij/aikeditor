package sample;

import javafx.application.Platform;
import sample.enums.TestRunnerStatus;
import sun.rmi.runtime.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.logging.Logger;

public class TestRunner implements Runnable{
    private static final String ERROR_DESCRIPTION_START_STRING = "Error: ";
    private static final String ERROR_MESSAGE_START_STRING = "Ошибка при исполнении \"%s\": ";
    private TestRunnerStatus testRunnerStatus;
    private String callText;
    private String programName;
    private Logger Log;

    public TestRunner(String scriptPath, String[] args, Logger Log, String programName) {
        testRunnerStatus = TestRunnerStatus.INIT;
        setCallText(args, scriptPath);
        this.programName = programName;
        this.Log = Log;
    }

    public TestRunner() {
    }

    public TestRunnerStatus getTestRunnerStatus() {
        return testRunnerStatus;
    }

    private void showFXMessage(String message){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {
                    Methods.messageDialog(message);
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        });
    }

    @Override
    public void run() {
        String s;
        testRunnerStatus = TestRunnerStatus.RUN;
        try{
            Process p = Runtime.getRuntime().exec(callText);

            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(p.getInputStream()));

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(p.getErrorStream()));

            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
                if (s.startsWith(String.format(ERROR_DESCRIPTION_START_STRING, programName))) {
                    showFXMessage(String.format(ERROR_MESSAGE_START_STRING, programName).concat(s));
                    testRunnerStatus = TestRunnerStatus.ERROR;
                }else{
                    System.out.println(s);
                }
            }

            int errorMessageStringsCount = 0;
            while ((s = stdError.readLine()) != null) {
                errorMessageStringsCount++;
                Log.info("Python Error: ".concat(s));
            }
            if (errorMessageStringsCount>0){
                showFXMessage(String.format("В процессе выполнения сценария среднего уровня возникла ошибка, смотреть \"%s\"", Log.getName()));
                testRunnerStatus = TestRunnerStatus.FAIL;
            }
        } catch (
        IOException e) {
            e.printStackTrace();
        }
        if (testRunnerStatus == TestRunnerStatus.RUN){
            testRunnerStatus = TestRunnerStatus.END;
        }
    }

    public void setCallText(String[] args, String scriptPath) {
        this.callText = "python " + scriptPath;
        for (String arg : args){
            callText+=" "+arg;
        }
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public void setLog(Logger log) {
        Log = log;
    }
}
