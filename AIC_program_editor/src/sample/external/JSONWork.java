package sample.external;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JSONWork {

    public static JSONObject getJsonObjectFromStringOrNull(String str){
        try {
            System.out.println(str);
            JSONParser jsonParser = new JSONParser();
            return (JSONObject) jsonParser.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getStringValueByKeyOrError(String JsonStr, String key){
        return getStringValueByKey(getJsonObjectFromStringOrNull(JsonStr), key);
    }

    public static String getStringValueByKey(JSONObject json, String key){
        if (json != null){
            return (String) json.get(key);
        }else return "Error";
    }

    public static Iterator getJsonArrayIterator(JSONObject jsonObject){
        JSONArray array = (JSONArray) jsonObject.clone();
        return array.iterator();
    }

    /*private static final String filePath = "C:\\Users\\katerina\\Desktop\\jsonTestFile.json";

    public static void main(String[] args) {

        try {
            // считывание файла JSON
            FileReader reader = new FileReader(filePath);

            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);

            // получение строки из объекта
            String firstName = (String) jsonObject.get("firstname");
            System.out.println("The first name is: " + firstName);

            // получение номера из объекта
            long id =  (long) jsonObject.get("id");
            System.out.println("The id is: " + id);

            // получение массива
            JSONArray lang= (JSONArray) jsonObject.get("languages");

            // берем элементы массива
            for(int i=0; i<lang.size(); i++){
                System.out.println("The " + i + " element of the array: "+lang.get(i));
            }
            Iterator i = lang.iterator();

            // берем каждое значение из массива json отдельно
            while (i.hasNext()) {
                JSONObject innerObj = (JSONObject) i.next();
                System.out.println("language "+ innerObj.get("lang") +
                        " with level " + innerObj.get("knowledge"));
            }
            // обрабатываем структуру в объекте
            JSONObject structure = (JSONObject) jsonObject.get("job");
            System.out.println("Into job structure, name: " + structure.get("name"));

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ParseException ex) {
            ex.printStackTrace();
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }

    }*/

}
