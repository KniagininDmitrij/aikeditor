package sample.simple;

public enum SourceTimeMode {
    SLOW, NORMAL, FAST
}
