package sample.simple;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import sample.enums.ExecutePointTypes;

public class ExecutingBounds {
    public static final String ORIGIN_ADDED_ATTRIBUTE_NAME = "boundStatus";
    public static final String ORIGIN_ADDED_START = "start";
    public static final String ORIGIN_ADDED_END = "end";
    public static final String ORIGIN_ADDED_NONE = "none";
    private static Node startNode;
    private static Node endNode;
    public static boolean hasStartNode = false;
    public static boolean hasEndNode = false;

    public static void setExecutePoint(ExecutePointTypes pointType, Node node) {
        switch (pointType){
            case BEGIN:
                if (!isStartNode(node)){
                    setStartNode(node);
                }else {
                    hasStartNode = false;
                }
                break;
            case END:
                if (!isEndNode(node)){
                    setEndNode(node);
                }else {
                    hasEndNode = false;
                }
                break;
        }
    }

    public static boolean isStartNode(Node node){
        if (hasStartNode){
            return node.equals(startNode);
        }
        return false;
    }

    public static boolean isEndNode(Node node){
        if (hasEndNode){
            return node.equals(endNode);
        }
        return false;
    }

    public static void setStartNode(Node node) {
        startNode = node;
        hasStartNode = true;
    }

    public static void setEndNode(Node node) {
        endNode = node;
        hasEndNode = true;
    }
    private static void markNodeBy(Node node, String originAdded){
        ((Element) node).setAttribute(ORIGIN_ADDED_ATTRIBUTE_NAME, originAdded);
    }
    private static boolean isNodeMarkedBy(Node node, String mark){
        try {
            return node.getAttributes().getNamedItem(ORIGIN_ADDED_ATTRIBUTE_NAME).getNodeValue().equals(mark);
        }catch (Exception exception){
            return false;
        }
    }
    public static Node getStartNode() {
        return startNode;
    }

    public static Node getEndNode() {
        return endNode;
    }

    public static boolean isHasStartNode() {
        return hasStartNode;
    }

    public static boolean isHasEndNode() {
        return hasEndNode;
    }

    public static void setHasStartNode(boolean hasStart) {
        hasStartNode = hasStart;
    }

    public static void setHasEndNode(boolean hasEnd) {
        hasEndNode = hasEnd;
    }
}
