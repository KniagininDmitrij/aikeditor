package sample.simple;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import sample.external.workWithXMLStructure;

public class PathHolder {
    private static final String CONFIG_FILE_PATH = ".aike";
    private static final String PATH_CONFIG_NODE_NAME = "paths";
    private static final String PRG_PATH_NODE_NAME = "prg";
    private static final String LIB_PATH_NODE_NAME = "lib";
    private static final String EXEC_PATH_NODE_NAME = "exec";
    private static final String DEFAULT_PRG_PATH = "C:\\TestInstruments\\AIK";
    private static final String DEFAULT_LIB_PATH = "no path";
    private static final String DEFAULT_EXEC_PATH = "C:\\TestInstruments\\AIK";

    private static Document configDocument;
    private static String programPath;
    private static String libPath;
    private static String execPath;

    static{
        init();
    }

    private static void init(){
        if (!tryToReadConfigFile()){
            fillConfigFileByDefaults();
        }
    }

    public static String getProgramPath() {
        if (programPath == null){
            init();
        }
        return programPath;
    }

    public static void setProgramPath(String programPath) {
        if (!programPath.equals(PathHolder.programPath)){
            PathHolder.programPath = programPath;
            savePathConfig();
        }
    }

    public static String getLibPath() {
        if(libPath == null){
            init();
        }
        return libPath;
    }

    public static void setLibPath(String libPath) {
        if (!libPath.equals(PathHolder.libPath)){
            PathHolder.libPath = libPath;
            savePathConfig();
        }
    }

    public static String getExecPath() {
        if (execPath == null){
            init();
        }
        return execPath;
    }

    public static void setExecPath(String execPath) {
        if (!execPath.equals(PathHolder.execPath)){
            PathHolder.execPath = execPath;
            savePathConfig();
        }
    }

    private static void fillConfigFileByDefaults() {
        libPath = (libPath == null) ? DEFAULT_LIB_PATH : libPath;
        programPath = (programPath == null) ? DEFAULT_PRG_PATH : programPath;
        execPath = (execPath == null) ? DEFAULT_EXEC_PATH : execPath;
        savePathConfig();
    }

    private static boolean tryToReadConfigFile() {
            try {
                if (configDocument == null) {
                    configDocument = workWithXMLStructure.createNewXMLDocument(CONFIG_FILE_PATH);
                }
                libPath = configDocument.getElementsByTagName(LIB_PATH_NODE_NAME).item(0).getTextContent();
                programPath = configDocument.getElementsByTagName(PRG_PATH_NODE_NAME).item(0).getTextContent();
                execPath = configDocument.getElementsByTagName(EXEC_PATH_NODE_NAME).item(0).getTextContent();
            } catch (Exception exception) {
                return false;
            }
        return true;
    }

    private static void savePathConfig(){
        try {
            if (configDocument == null ||
                    configDocument.getElementsByTagName(PATH_CONFIG_NODE_NAME).getLength() == 0 ) {
                if (configDocument == null){
                    configDocument = workWithXMLStructure.createNewXMLDocument();
                }
                Node pathConfigNode = configDocument.createElement(PATH_CONFIG_NODE_NAME);
                configDocument.appendChild(pathConfigNode);
                pathConfigNode.appendChild(getTextElement(LIB_PATH_NODE_NAME, DEFAULT_LIB_PATH));
                pathConfigNode.appendChild(getTextElement(PRG_PATH_NODE_NAME, DEFAULT_PRG_PATH));
                pathConfigNode.appendChild(getTextElement(EXEC_PATH_NODE_NAME, DEFAULT_EXEC_PATH));
            }else{
                configDocument.getElementsByTagName(LIB_PATH_NODE_NAME).item(0).setTextContent(libPath);
                configDocument.getElementsByTagName(PRG_PATH_NODE_NAME).item(0).setTextContent(programPath);
                configDocument.getElementsByTagName(EXEC_PATH_NODE_NAME).item(0).setTextContent(execPath);
            }
            workWithXMLStructure.saveXMLDocument2FileByFilePath(configDocument, CONFIG_FILE_PATH);
        }catch (Exception exception){

        }
    }

    private static Node getTextElement(String tegName, String text) {
        Node newNode = configDocument.createElement(tegName);
        newNode.setTextContent(text);
        return newNode;
    }
}
