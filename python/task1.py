def forFXML():
	f2 = open('output.txt', 'w')

	for i in range(16):
		f2.write("""<Line endX="-100.0" endY="450.0" layoutX="{0}" layoutY="80.0" startX="-100.0" />
	<Line endX="-100.0" endY="450.0" layoutX="{1}" layoutY="80.0" startX="-100.0" />
	<Label contentDisplay="CENTER" layoutX="{2}" layoutY="520" prefHeight="17.0" prefWidth="87.0" text="{4}" rotate="90">
        <font>
            <Font size="12.0" />
        </font>
    </Label>
    <Label contentDisplay="CENTER" layoutX="{3}" layoutY="520" prefHeight="17.0" prefWidth="87.0" text="{5}" rotate="90">
        <font>
            <Font size="12.0" />
        </font>
    </Label>\n""".format(120+35*(i),135+35*i,20-14-24+35*(i),35-14-24+35*i,"C"+str(i)+"+","C"+str(i)+"-"))

	f2.write("\n")
	for i in range(8):
		f2.write("""<Line endX="480.0" layoutX="100.0" layoutY="{0}" startX="-100.0" />
	<Line endX="480.0" layoutX="100.0" layoutY="{1}" startX="-100.0" />
	<Label contentDisplay="CENTER" layoutX="-4.0" layoutY="{2}" prefHeight="17.0" prefWidth="87.0" text="{4}">
        <font>
            <Font size="12.0" />
        </font>
    </Label>
    <Label contentDisplay="CENTER" layoutX="-4.0" layoutY="{3}" prefHeight="17.0" prefWidth="87.0" text="{5}">
        <font>
            <Font size="12.0" />
        </font>
    </Label>\n""".format(110+50*(i),125+50*i,110-14+50*(i),125-14+50*i,"R"+str(i)+"+","R"+str(i)+"-"))

	f2.write("\n")
	for i in range(8):
		for j in range(16):
			f2.write("""<Circle fx:id="node_{0}_1" fill="#fdfeff" layoutX="{1}" layoutY="{2}" radius="5.0" stroke="BLACK" strokeType="INSIDE" />
	<Circle fx:id="node_{0}_2" fill="#fdfeff" layoutX="{3}" layoutY="{4}" radius="5.0" stroke="BLACK" strokeType="INSIDE" />\n""".format(('r'+str(i)+'c'+str(j)),20+35*j,110+50*(i),35+35*(j),125+50*i))
	f2.close()

def forFXML_():
	f2 = open('output.txt', 'w')

	for i in range(8):
		f2.write("""<Line endX="-100.0" endY="670.0" layoutX="{0}" layoutY="80.0" startX="-100.0" />
	<Line endX="-100.0" endY="670.0" layoutX="{1}" layoutY="80.0" startX="-100.0" />
	<Label contentDisplay="CENTER" layoutX="{2}" layoutY="770" prefHeight="17.0" prefWidth="87.0" text="{4}" rotate="90">
        <font>
            <Font size="12.0" />
        </font>
    </Label>
    <Label contentDisplay="CENTER" layoutX="{3}" layoutY="770" prefHeight="17.0" prefWidth="87.0" text="{5}" rotate="90">
        <font>
            <Font size="12.0" />
        </font>
    </Label>
    <Label contentDisplay="CENTER" layoutX="{2}" layoutY="115" prefHeight="17.0" prefWidth="87.0" text="{4}" rotate="90">
        <font>
            <Font size="12.0" />
        </font>
    </Label>
    <Label contentDisplay="CENTER" layoutX="{3}" layoutY="115" prefHeight="17.0" prefWidth="87.0" text="{5}" rotate="90">
        <font>
            <Font size="12.0" />
        </font>
    </Label>\n""".format(120+35*(i)+10*(i//4),135+35*i+10*(i//4),20-14-24+35*(i)+10*(i//4),35-14-24+35*i+10*(i//4),"R"+str(i)+"+","R"+str(i)+"-"))

	f2.write("\n")
	labelSMU_list = [["SHI", "HI"], ["SLO", "LO"]]
	daqLabelList = [["AI 0", "AI GND"],["AI GND", "AI 1"],["AO 0", "AO GND"],["AO 1", "AO GND"],["",""],["PFI 0", "D GND"],["PFI 6", "D GND"],["PFI 9", "D GND"]]
	for i in range(16):
		f2.write("""<Line endX="200.0" layoutX="100.0" layoutY="{0}" startX="-200.0" />
	<Line endX="200.0" layoutX="100.0" layoutY="{1}" startX="-200.0" />
	<Label contentDisplay="CENTER" layoutX="-4.0" layoutY="{2}" prefHeight="17.0" prefWidth="87.0" text="{4}">
        <font>
            <Font size="12.0" />
        </font>
    </Label>
    <Label contentDisplay="CENTER" layoutX="-4.0" layoutY="{3}" prefHeight="17.0" prefWidth="87.0" text="{5}">
        <font>
            <Font size="12.0" />
        </font>
    </Label>
    <Label contentDisplay="CENTER" layoutX="-80.0" layoutY="{2}" prefHeight="17.0" prefWidth="87.0" text="{6}">
        <font>
            <Font size="12.0" />
        </font>
    </Label>
    <Label contentDisplay="CENTER" layoutX="-80.0" layoutY="{3}" prefHeight="17.0" prefWidth="87.0" text="{7}">
        <font>
            <Font size="12.0" />
        </font>
    </Label>\n""".format(110+35*(i)+10*(i//2),125+35*i+10*(i//2),110-14+35*(i)+10*(i//2),125-14+35*i+10*(i//2),"C"+str(i)+"+","C"+str(i)+"-",labelSMU_list[i%2][0]+str(i//2+1) if i< 8 else daqLabelList[(i-8)][0], labelSMU_list[i%2][1]+str(i//2+1) if i< 8 else daqLabelList[(i-8)][1]))

	f2.write("\n")
	for i in range(16):
		for j in range(8):
			f2.write("""<Circle fx:id="node_{0}_1" fill="#fdfeff" layoutX="{1}" layoutY="{2}" radius="5.0" stroke="BLACK" strokeType="INSIDE" />
	<Circle fx:id="node_{0}_2" fill="#fdfeff" layoutX="{3}" layoutY="{4}" radius="5.0" stroke="BLACK" strokeType="INSIDE" />\n""".format(('r'+str(j)+'c'+str(i)),20+35*j+10*(j//4),110+35*(i)+10*(i//2),35+35*(j)+10*(j//4),125+35*i+10*(i//2)))
	f2.close()

def forRelay(t):
	f2 = open('output.txt', 'w', encoding = "utf-8")
	if t==1:
		for i in range(8):
			for j in range(16):
				f2.write("""@FXML
public Circle node_{0}_1;
@FXML
public Circle node_{0}_2;\n""".format(('r'+str(i)+'c'+str(j))))
	elif t==2:
		for i in range(8):
			for j in range(16):
				f2.write("""relay r_{0} = new relay("Реле_{0}", "b{1}{2}");\n""".format(('r'+str(i)+'c'+str(j)),0 if i<4 else 1,('r'+str(i%4)+'c'+str(j))))
	elif t==3:
		for i in range(8):
			for j in range(16):
				f2.write("""relayOnMouseClicked(r_{0});\n""".format(('r'+str(i)+'c'+str(j))))
	elif t==4:
		for i in range(8):
			for j in range(16):
				f2.write("""r_{0}.setNodes(new Circle[] {{node_{0}_1,node_{0}_2}});
relaysMap.put(r_{0}.getRelay_id(),r_{0});\n""".format(('r'+str(i)+'c'+str(j)),'{'))		
	f2.close()

def forLines(t):
	startPoints = [[10.129547119140625-33.701175689697266,19.83686065673828+18.152572631835938],[10.12954711914062-33.701175689697266,56.83685302734375+18.152572631835938],
	[10.12954711914062-33.701175689697266,90.83685302734375+18.152572631835938],[10.12954711914062-33.701175689697266,132.83685302734375+18.152572631835938]] 
	highLineY = 30
	stepl2l = 6
	f2 = open('output.txt', 'w', encoding = "utf-8")
	if t==1:
		for i in range(len(startPoints)):
			f2.write("""<Line endX="{2}" layoutX="{0}" layoutY="{1}" startX="0" />
<Line endX="{2}" layoutX="{0}" layoutY="{1}" endY = "{3}" startX="{2}" />\n
<Line endX="44" layoutX="-18.571628570556648" layoutY="155.9894256591797" endY = "-185.9894256591797" startY = "0" startX="44" />""".format(startPoints[i][0]+5,startPoints[i][1]+5, 20+stepl2l*(i+1),highLineY))
	if t==2:
		for i in range(8):
			f2.write("""<Line endX="0" layoutX="{0}" layoutY="0" endY = "{1}" startX="0" />
<Line endX="{2}" layoutX="{0}" layoutY="{1}" startX="0" />
""".format(7*15-i*15,43+7*i,250-10*i))
	if t==3:
		for i in range(17):
			f2.write("""<Line startX="0" endX="270" layoutX="90" layoutY="{0}"/>\n""".format(13+i*21.76))
	if t==4:
		for i in range(17):
			f2.write("""<Label contentDisplay="CENTER" layoutX="250.0" layoutY="{0}" prefHeight="17.0" prefWidth="87.0" text="{1}">
 	<font>
    	<Font size="12.0" />
 	</font>
</Label>\n""".format(-2+i*21.76, "some text"))
	f2.close()

#forFXML_()
forLines(4)