path = 'D:\\Documents\\aik_edit_prog\\builds\\tetsPRJ\\My DLL\\SharedLib.dll'
#path = 'C:\\Users\\AIK\\Desktop\\new folder\\My DLL\\SharedLib.dll'
import ctypes
import numpy as np
import json, math
def XMLWorker(DLLPath, XMlPath, bufferSize):
	newlib = ctypes.CDLL(DLLPath)
	newlib.XMLWorker.argtypes = [ctypes.POINTER(ctypes.c_char), ctypes.POINTER(ctypes.c_char), ctypes.c_int]
	newlib.XMLWorker.restype = ctypes.c_int
	outstr = np.full(bufferSize, b' ')
	bufferout = (ctypes.c_char * len(outstr))(*outstr)
	Path = bytes(XMlPath, encoding='utf8')

	BPath = (ctypes.c_char * len(Path))(*Path)
	error = newlib.XMLWorker(BPath,bufferout,bufferSize)
	if error:
		print("Error: {}".format(error))
	outputdata = np.frombuffer(bufferout, dtype='S1')

	result = []
	for i in range(0, len(outputdata)):
		result.append(str(outputdata[i].decode('utf8')))

	resultStr = ''.join(result).rstrip()
	result, vacs = [json.loads(i) for i in resultStr.split('###')]
	
	resultDict = []
	for i in result:
		tempD = {}
		try:
			exec("tempD['value'] ="+i[0])
		except ZeroDivisionError as e:
			tempD['value'] = "NaN"	
		tempD['unit'] = i[1]
		tempD['normed'] = i[2]!="-" and i[3]!="-"
		if tempD['normed']:
			tempD['minNorm'] = float(i[2])
			tempD['maxNorm'] = float(i[3])
		resultDict.append(tempD)
	print(resultDict)
	print(vacs)
	plot(vacs)

import matplotlib.pyplot as plt

def plot(vacs):
	L = len(vacs)
	placeNx = int(L**(0.5))+1
	placeNy = L//placeNx+1
	plt.figure(figsize=(4*placeNx,4*placeNy))
	print(4*placeNx,4*placeNy)
	for j in range(L):
		i=vacs[j]
		if i.get('needPlot') == 'yes':
			plt.subplot(placeNx, placeNy, j+1)
			x = [i[0] for i in i.get("points")]
			y = [i[1] for i in i.get("points")]
			plt.plot(x,y)
			plt.title("ВАХ")
			plt.xlabel(i.get('labelX')+', '+i.get('unitX')) # ось абсцисс
			plt.ylabel(i.get('labelY')+', '+i.get('unitY')) # ось ординат
			plt.grid(True)      # включение отображение сетки
	plt.tight_layout(pad=1.1)
	plt.show()
XMLWorker(path, 'D:\\Documents\\aik_edit_prog\\testLibXML\\somePrg.xml',50000)
#XMLWorker(path, 'C:\\Users\\AIK\\Desktop\\new folder\\somePrg.xml',20000)


